#!/bin/bash

########## Variables

# These variables are used for bold and normal text
bl=''
nm=''
# They are only set if stdout is a terminal (file descriptor 1 is open on a terminal)
# This is because if the output is redirected to another instance of itself or sequence,
# it will mess up the terminal when it processes text with these codes
if [ -t 1 ]; then
  bl=$(tput bold)
  nm=$(tput sgr0)
fi

HELP_TEXT="
Cipher script
Written by Stephen Kendall

Capable of deciphering and enciphering with 
  Caesar, Atbash, A1Z26, Vigenere, Beaufort, Keyword, Playfair, and Bifid.
The Vigenere implementation supports the Beaufort, Gronsfeld, and Autokey variations.
If multiple ciphers are requested, the same input string will be processed by each one separately."

# URL to use to generate text
GENURL="https://randomword.com/paragraph"

HELP_FLAGS="
Script Modifiers:
  -h, --help            show this help message and exit
  -i FILE, --input FILE
                        file containing text to process
                        if this flag is not set, it will read from standard input through redirection or keyboard input
  -g, --generate        Generate the input text from '$GENURL'
  -v, --verbose         enable more detailed output (-v=info, -vv=tutorial, -vvv=step-by-step)
  --runtime             print detailed execution times (for debug)
  -c CASE, --case CASE  convert output string to the specified case
  -s, --strip, --strip-spaces
                        strip space characters from output string
  -y, --yes, --assume-yes
                        silently continue if the input string doesn't seem to match the cipher
  -n, --no, --assume-no
                        silently skip cipher; if neither specified, will ask interactively

Cipher Selectors (at least one required):
  -C, --caesar          use the Caesar cipher, default to shift right 3 characters
  -O DIST, --offset DIST
                        use the Caesar cipher with the specified distance, inplies '-C'
  -A, --atbash          use the Atbash cipher
  -Z, --a1z26           use the A1Z26 cipher
  -V VKEY, -G VKEY, --vigenere VKEY, --gronsfeld VKEY
                        use the Vigenere cipher or a variant
  -U UKEY, --autokey UKEY
                        use the Vigenere Autokey cipher
  -B BKEY, --beaufort BKEY
                        use the Beaufort cipher
  -K KKEY, --keyword KKEY
                        use the Keyword cipher
  -P PKEY, --playfair PKEY
                        use the Playfair cipher
  -I IKEY, --bifid IKEY
                        use the Bifid cipher
  -S SUBS, --substitute SUBS, --substitution SUBS
                        provide specific monoalphabetic substitutions to perform
  -R, --random          randomly generate and apply a substitution alphabet

Cipher Modifiers:
  -b, --brute, --brute-force
                        modify Caesar to print every shift up to DIST (default 3)
  -r, --right           use a right shift for Caesar and Vigenere ciphers (default)
  -l, --left            use a left shift for Caesar and Vigenere ciphers
  -e, --encipher        encipher input text (default)
  -d, --decipher        decipher input text
  -p PERIOD, --period PERIOD
                        specify the period for Bifid to intersperse y- and x-values (deafult 5) 

Consult the Wiki for more details: https://gitlab.com/SirDoctorK/cipher/-/wikis/home"

########## Functions

##### Supporting functions

# Return the name of the script
cmd () { echo $(basename $0); }

# Return current time in seconds.milliseconds
now () {
  date +%s.%N
}

# Compute elapsed runtime based on provided start time
runtime () {
  start=$1
  desc="$2"
  end=$(now)
  if $RUNTIME; then echo "     - $desc took $( echo "$end - $start" | bc -l ) seconds"; fi
  #if $RUNTIME; then echo "$end - $start" | bc -l; fi
}

# This just contains a bunch of variable initializations
init_vars () {
  # tr doesn't work with ranges in reverse order ("z-a"), so we have variables containing
  # the whole alphabet in reverse order (and correct order so the code is consistent)
  az="$(echo {a..z} | tr -d ' ')"       # abcd...
  AZ="$(echo {A..Z} | tr -d ' ')"       # ABCD...
  za="$(echo {z..a} | tr -d ' ')"       # zyxw...
  ZA="$(echo {Z..A} | tr -d ' ')"       # ZYXW...

  # Construct Caesar substitution strings so we can just do one 'tr' command for any offset
  azAZ="$az$AZ"
  czArr=( "$azAZ" )
  # Generate all Caesar alphabets
  for i in {1..25}; do
    # Each increasing Caesar alphabet is another rotation of the previous
    czArr[$i]=$(tr "A-Za-z" "B-ZAb-za" <<< ${czArr[$i-1]})
  done

  # Initialize an alphabet array with empty first index for two reasons:
  # 1. This puts 'a' at index 1, so we can directly use A1Z26 numbers as the array index
  # 2. This allows appending alpha[0] to the resulting string ad nauseam with no consequences
  alpha=( '' {a..z} )

  # Control variables
  ASSUME=nothing                        # Used to assume Y or N to strings that don't match the cipher
  VERBOSE=0                             # Verbosity level (3 levels)
  RUNTIME=false                         # Print runtime information
  CASE=''                               # Enforce upper/lower-case output
  STRIP=false                           # Remove spaces from input string

  # Caesar function variables
  CAE=false                             # To perform Caesar (offset)
  DIST=3                                # Distance of shift
  DIRECTION=''                          # Direction of shift (right assumed if not specified)
  BR=false                              # Whether to report progress on each offset up to the distance (brute force mode)

  # Atbash function variables
  ATB=false                             # To perform Atbash (Reverse)

  # A1Z26 function variables
  A1Z=false                             # To perform A1Z26 cipher (Number)

  # Vigenere function variables (also uses DIRECTION variable above)
  VIG=false                             # To perform Vigenere
  VVKEY=''                              # Key for Vigenere and variants
  AK=false                              # To use autokey variant
  OP=''                                 # Operation for Autokey, Keyword, Playfair, Bifid

  # Beaufort function variables
  BEA=false                             # To perform Beaufort
  BKEY=''                               # Key for Beaufort

  # Keyword function variables
  KW=false                              # To perform Keyword
  KKEY=''                               # Key for Keyword

  # Playfair function variables
  PF=false                              # To perform Playfair
  PKEY=''                               # Key for Playfair

  # Bifid function variables
  BIF=false                             # To perform Bifid
  IKEY=''                               # Key for Bifid
  PERIOD=5                              # Period (frequency with which y- and x-values are interspersed)

  # Custom substitution variables
  SUB=false                             # To perform custom substitution
  SUBARGS=( )                           # Array for arguments to the substitution function

  # Random ciphertext variables
  GEN=false                             # To generate the input text
  RAND=false                            # To randomize cipher operations
}

# Initialize starting string from input file, stdin, or random paragraph webpage
init_str () {
  # variable to be used as the resulting string
  rstr=''

  # If set to generate input text, fetch that here
  if $GEN; then
    if [[ $VERBOSE > 0 ]]; then
      echo "Fetching generated input text from '$GENURL'"
    fi
    # Fetch the random paragraph with silent curl and:
    # 1. Grab the first line that matches the correct div id
    # 2. Remove opening and closing div tags (has to be specific because sed is greedy)
    # 3. Remove leading/trailing whitespace
    istr=$(curl -s $GENURL | grep -m 1 "random_word_definition" | sed -e 's/<.*">//g' -e 's/<\/.*>//g' | awk '{$1=$1};NF')
    if [[ $istr == '' ]]; then
      echo "Couldn't get random paragraph as input text" > /dev/stderr
    fi

  # If not set to generate input text, read from normal methods
  else
    if [[ $VERBOSE > 0 ]]; then
      if [ ! $fname ]; then
        echo "No filename, reading from stdin ('${bl}Ctrl-D${nm}' to detach if interactive)"
      else
        echo "Reading from ${bl}$fname${nm}"
      fi
    fi
    # if filename is empty, reads from stdin
    # if cat throws an error, exits script
    if ! istr=$(cat $fname); then exit 1; fi   # original string
  fi

  # Check and exit script if the input string is empty
  if [[ $istr == '' ]]; then
    echo "Empty input string; nothing to do." > /dev/stderr
    exit 1
  fi

  # Any verbosity causes the original input string to be printed
  if [[ $VERBOSE > 0 ]]; then echo -e "  Input string:\n$istr"; fi
}

# Print out brief description of the script and available command line arguments
usage () {
  # An argument being passed inidicates a syntax error in the command
  if [[ $1 != "" ]]; then
    # 'noop' inidcates that no cipher operation was specified
    if [[ $1 == "noop" ]]; then
      echo "No cipher operation specified." > /dev/stderr
      echo "Use at least one of '-[$(echo $opts | tr -d : | grep -oE [A-Z]+)]' to select an operation." > /dev/stderr
    # 'noarg' indicates that there was no argument where expected (or a different getopts error)
    # In either case, the error message is taken care of before calling the usage function
    # If it is neither 'nooop', 'case', nor 'noarg', then $1 contains an invalid option
    elif [[ $1 != "noarg" ]]; then
      echo "$(cmd): invalid option -- '$1'" > /dev/stderr
    fi
    echo "Run '$(cmd) -h' for more information." > /dev/stderr
    exit 1
  fi

  # Default operation of usage is just to report general script and flags description
  echo -e "$HELP_TEXT"
  echo -e "$HELP_FLAGS"
}

# Function that parses command line arguments
get_args () {
  # process command line arguments and override variables as needed
  # method for flag parsing from here: https://gist.github.com/shakefu/2765260

  # list of single-letter flags supported
  # : = positional argument required
  opts="hi:gvc:synCO:AZV:G:U:B:K:P:I:SRblrdep:"

  for pass in 1 2; do
    while [ -n "$1" ]; do
      case $1 in
        --) 
          shift
          # Grab all remaining arguments into SUBARGS
          while [ -n "$1" ]; do
            SUBARGS+=($1)
            shift
          done
          break
          ;;
        -*) case $1 in
          -h | --help )
            usage
            exit
            ;;
          -i | --input )
            shift
            fname=$1
            ;;
          -g | --generate )
            GEN=true
            ;;
          -v | --verbose )
            (( VERBOSE++ ))
            ;;
          --runtime )
            RUNTIME=true
            ;;
          -c | --case )
            shift
            # This will assign the CASE variable and check the return status
            if ! CASE=$(canoncase $1); then
              usage "case"
            fi
            ;;
          -s | --strip | --strip-spaces )
            STRIP=true
            ;;
          -y | --yes | --assume-yes )
            ASSUME=Y
            ;;
          -n | --no | --assume-no )
            ASSUME=N
            ;;
          -C | --caesar )
            CAE=true
            ;;
          -O | --offset )
            shift
            if [[ $1 =~ ^[0-9]+$ ]]; then 
              DIST=$1
              CAE=true
            else 
              echo "Need to specify offset distance." > /dev/stderr
              usage noarg
            fi
            ;;
          -A | --atbash )
            ATB=true
            ;;
          -Z | --a1z26 )
            A1Z=true
            ;;
          -V | -G | --vigenere | --gronsfeld  )
            VIG=true
            shift
            VKEY=$1
            ;;
          -U | --autokey  )
            VIG=true
            AK=true
            shift
            VKEY=$1
            # Set decipher operation if not already set
            # Allows '-d' and '-e' to take effect if placed before this flag
            if [[ $OP == '' ]]; then OP='en'; fi
            ;;
          -B | --beaufort )
            BEA=true
            shift
            BKEY=$1
            ;;
          -K | --keyword )
            KW=true
            shift
            KKEY=$1
            ;;
          -P | --playfair )
            PF=true
            shift
            PKEY=$1
            ;;
          -I | --bifid )
            BIF=true
            shift
            IKEY=$1
            ;;
          -S | --substitute | --substitution )
            SUB=true
            ;;
          -R | --random )
            RAND=true
            ;;
          -b | --brute | --brute-force )
            BR=true
            ;;
          -r | --right )
            DIRECTION=R
            ;;
          -l | --left )
            DIRECTION=L
            ;;
          -e | --encipher )
            OP='en'
            # Set direction if not already set
            # Allows '-l' and '-r' to take effect if placed before this flag
            if [[ $DIRECTION == '' ]]; then DIRECTION=R; fi
            ;;
          -d | --decipher )
            OP='de'
            if [[ $DIRECTION == '' ]]; then DIRECTION=L; fi
            ;;
          -p | --period )
            shift
            if [[ $1 =~ ^[0-9]+$ ]]; then
              PERIOD=$1
            else
              echo "Invalid period $1; will run with default of $PERIOD." > /dev/stderr
            fi
            ;;
          --*)
            usage $1
            exit
            ;;
          -*)
            if [ $pass -eq 1 ]; then ARGS="$ARGS $1";
            else usage $1; fi;;
          esac;;
        *)  
          if [ $pass -eq 1 ]; then ARGS="$ARGS $1";
          else usage $1; fi;;
      esac
      shift
    done
    if [ $pass -eq 1 ]; then 
      ARGS=$(getopt $opts $ARGS)
      if [ $? != 0 ]; then usage noarg; fi
      set -- $ARGS
    fi
  done

  # If none of these are true, then no operation has been specified
  # Check for this before reading the input string
  if ! $CAE && ! $ATB && ! $A1Z && ! $VIG && ! $BEA && 
        ! $KW && ! $PF && ! $BIF && ! $SUB && ! $RAND; then
    usage noop
  fi

  # Set default direction if unset
  if [[ $DIRECTION == '' ]]; then DIRECTION=R; fi

  # If capital letters are requested, we will translate lowercase to uppercase
  if [[ $CASE == C ]]; then SET1='[:lower:]'; SET2='[:upper:]'
  # If small letters are requested, we will translate uppercase to lowercase
  elif [[ $CASE == S ]]; then SET1='[:upper:]'; SET2='[:lower:]'
  # If no case is requested, we will translate uppercase to uppercase
  else SET1='[:upper:]'; SET2='[:upper:]'
  fi
}

# Identify whether string contains primarily digits or alpha
contains () {
  # Identify the contents by the first or second character
  if [[ "$istr" =~ ^.?[0-9] ]]; then echo "digits"
  elif [[ "$istr" =~ ^.?[A-Za-z] ]]; then echo "letters"
  else echo "unknown"
  fi
}

# Canonicalize case designator
canoncase () {
  # Using regex match to see if the provided case (changed to lowercase)
  # matches the start of a valid designator
  if [[ "capital" =~ ^${1,,} ]] || [[ "uppercase" =~ ^${1,,} ]]; then
    echo C
  elif [[ "small" =~ ^${1,,} ]] || [[ "lowercase" =~ ^${1,,} ]]; then
    echo S
  elif [[ "none" =~ ^${1,,} ]] || [[ "original" =~ ^${1,,} ]]; then
    echo N
  else
    echo "Invalid case designator '$1'" > /dev/stderr
    echo "Specify all or part of 'capital', 'uppercase', 'small', or 'lowercase'" > /dev/stderr
    return 1
  fi
}

# Print warning if string doesn't seem to work with desired cipher
confirm () {
  # If an assumption has been specified, return from the function with the appropriate return value
  if [ $ASSUME == "Y" ]; then return; 
  elif [ $ASSUME == "N" ]; then return 1;
  else
    echo -e "String starts with $CONT, which the specified cipher doesn't use - unlikely to be useful." > /dev/stderr
    echo -e "(Add '-y' or '-n' to suppress this message.)" > /dev/stderr
    return
  fi
}

# Return the index of the provided character
get_index () {
  # If the provided character is a digit, return it (for Vigenere Gronsfeld variant)
  if [[ $1 =~ ^[0-9]$ ]]; then
    # We need to return $1 plus one since the vigenere() function decrements this result
    # and when using Gronsfeld, we need 0 and 1 to work directly
    echo $(($1 + 1))
    return
  fi
  
  # Loop through all characters in the alphabet
  # Using $j since this function is often called within another loop using $i
  for j in ${!alpha[@]}; do 
    # The ${1,,} construct gives the lower-case version of the first function parameter
    if [ "${alpha[$j]}" == "${1,,}" ]; then 
      # Typically this echo statement is caught by a variable and not printed to stdout
      echo $j
      return
    fi
  done
}

# Initialize alphabet with specific options for Keyword, Playfair, and Bifid
alphinit () {
  # Reinitialize alphabet array to standard sequence
  alpha=( '' {a..z} )
  # Array index of the start of the untouched alphabet
  # (Alphabet Start Index)
  # All functions that use this pop off the 0th element
  asi=0

  # Argument to the function tells it which rules to use to construct the alphabet
  # It will grab the lower case version of that key
  if [[ $1 == 'b' ]]; then
    # All we need for Beaufort is an alphabet array without an empty 0th element
    alpha=( {a..z} )
  elif [[ $1 == 'k' ]]; then
    tempkey=${KKEY,,}
    alpha=( {a..z} )
  elif [[ $1 == 'p' ]]; then
    # Playfair needs 'j' turned to 'i' in both key and istr
    tempkey="$(tr j i <<< ${PKEY,,})"
    # Re-initialize alpha[] without j or starting empty element
    alpha=( {a..i} {k..z} )
  elif [[ $1 == 'i' ]]; then
    # Bifid uses same alphabetic initialization rules, but with a separate key
    tempkey="$(tr j i <<< ${IKEY,,})"
    alpha=( {a..i} {k..z} )
  else # If no argument, leave alpha[] as default
    return
  fi

  # Construct the substitution alphabet
  # Loop through characters in the key (by default the key is empty)
  for (( i=0; i<${#tempkey}; i++ )); do
    # Get one character from the key at position $i
    k=${tempkey:$i:1}
    # Verify that $k is a letter
    if [[ $k =~ ^[a-z]$ ]]; then
      # Get the index of the key character
      kin=$(get_index $k)
      # If the key character index is at or past the alphabetic start index,
      # we are at its first occurrence in the key
      if (( "$kin" >= "$asi" )); then
        # Loop from $kin down to $asi (if equal, will do nothing)
        for (( j=$kin; j>$asi; j-- )); do
          # Move the characters right one space in this range
          alpha[$j]=${alpha[$j-1]}
        done
        # Set the alphabet at $asi to the current key character
        alpha[$asi]=$k
        # Increment $asi
        (( asi++ ))
      fi
      # If this is not the first occurrence of the key character, nothing to do
    fi
    # If $k is not a letter, we don't need to do anything with it
  done
}

# Print current alphabet in 2d for Playfair / Bifid (for debugging)
print2d () {
  echo "  0 1 2 3 4"
  echo 0 ${alpha[0]} ${alpha[1]} ${alpha[2]} ${alpha[3]} ${alpha[4]}
  echo 1 ${alpha[5]} ${alpha[6]} ${alpha[7]} ${alpha[8]} ${alpha[9]}
  echo 2 ${alpha[10]} ${alpha[11]} ${alpha[12]} ${alpha[13]} ${alpha[14]}
  echo 3 ${alpha[15]} ${alpha[16]} ${alpha[17]} ${alpha[18]} ${alpha[19]}
  echo 4 ${alpha[20]} ${alpha[21]} ${alpha[22]} ${alpha[23]} ${alpha[24]}
}

# Get element from array with 2d indices
get2d () {
  # Ensure the parameters are negative or positive digits before proceeding
  if ! [[ $1 =~ ^-?[0-9]$ ]] || ! [[ $2 =~ ^-?[0-9]$ ]]; then
    echo "get2d($1 $2): Invalid parameters" > /dev/stderr
    return
  fi

  # Perform modulo to account for positive rollover
  ix=$(( $1 % 5 ))
  iy=$(( $2 % 5 ))

  # Account for negative X-value rollover
  if (( "$1" < "0" )); then
    # The X or Y value should be 5 "plus" the negative number
    # E.g., 5 + -1 = 5 - 1 = 4 (last column)
    ix=$(( 5 + $1 ))
  fi

  # Same for negative Y-value rollover
  if (( "$2" < "0" )); then
    iy=$(( 5 + $2 ))
  fi
  
  # Observe the layout in the print2d() function if this doesn't make sense
  index=$(( $iy * 5 + $ix ))

  # The "return value" captured when the function is called
  echo ${alpha[$index]}
}

# Wait for Enter to continue, used in tutorials
wait_enter() {
  # If no argument provided, assume solving the cipher
  if [[ -z "$1" ]]; then
    ACTION="Solve the cipher"
  # Argument to function, if provided, is the action that we should suggest the user to do
  else
    ACTION="$1"
  fi

  # Try to wait for Enter to continue, else just print a newline and continue
  if read -p "$ACTION on your own if you wish, then press ${bl}Enter${nm} to continue: " -r; then
    true
  else # If `read` fails, it's non-interactive and we'll assume no
    echo
  fi
  # Extra newline to get the spacing right
  echo
}

##### Core functions

# Caesar cipher (default to 3-char offset)
caesar () {
  # If a parameter is provided to the function, it will offset the provided string (for Vigenere and tutor function)
  if [ "$1" != "" ]; then rstr=$1
  # Otherwise it will offset $istr
  else rstr="$istr"
  fi

  #cae_start=$(now)

  if $BR; then
    # If using brute force mode, the max distance should be enforced by setting anything higher to 25
    if [[ $DIST > 25 ]]; then DIST=25; fi
    # This is the original logic, which is still necessary for the brute force functionality
    if [[ $DIRECTION == "L" ]]; then
      # Using $j since this function is sometimes called within another loop using $i
      for (( j=1; j<=$DIST; j++ )); do
        # Shift characters left
        rstr="$(tr "A-Za-z" "ZA-Yza-y" <<< $rstr)"
        # If we're doing a brute force run, print current offset
        if $BR; then echo "Offset $j:"; report; fi
      done
    else # If no direction specified, assume right (encipher)
      for (( j=1; j<=$DIST; j++ )); do
        # Shift characters right
        rstr=$(tr "A-Za-z" "B-ZAb-za" <<< $rstr)
        # If we're doing a brute force run, print current offset
        if $BR; then echo "Offset $j:"; report; fi
      done
    fi
  else
    # This is the new logic, which requires only 1 'tr' command per function call.
    # This dramatically improves performance, expecially when the Vigenere calls this function.

    # Offset 26 is equivalent to 0
    # To make this new Caesar approach work, it can't be higher than 25
    DIST=$(( $DIST % 26 ))

    #tr_start=$(now)

    # Depending on direction, translate from or to the correct Caesar alphabet
    if [[ $DIRECTION == "L" ]]; then
      rstr=$(tr "${czArr[$DIST]}" "$azAZ" <<< $rstr)
    else
      rstr=$(tr "$azAZ" "${czArr[$DIST]}" <<< $rstr)
    fi

    #runtime $tr_start "running tr command"
  fi
  #runtime "$cae_start" "processing Caesar with offset $DIST"
}

# Caesar tutorial
caesar_tutor () {
  echo
  echo "== Caesar Cipher tutorial =="
  echo
  echo "The Ceasar Cipher shifts all letters in the string by DIST positions in the alphabet."
  echo "The default Caesar distance is 3."
  echo "Typically a RIGHT shift is used to encipher text and LEFT to decipher."
  echo
  echo "To perform the Caesar cipher with the current settings,"
  echo " 1. Locate each character in the input string"
  echo " 2. Replace it with the character that is $DIST spaces to the $DIRECTION:"
  echo
  # Right justify 10 spaces
  printf "%10s" "input"
  echo " = $istr"
  printf "%10s" "operation"
  echo " = $DIRECTION $DIST"
  echo

  # Print out normal alphabet 
  echo {a..z}
  echo

  wait_enter

  # If verbosity is over 2, we will step by step explain each word in the string
  if [[ $VERBOSE > 2 ]]; then
    # If an instr has been provided, set it to istr
    if [ "$1" != "" ]; then instr=$1
    # Otherwise set it to $istr
    else instr="$istr"
    fi

    # Using c-style loop to iterate over individual characters in $istr
    for (( i=0; i<${#istr}; i++ )); do 
      # Get one character from $istr at position $i
      c=${istr:$i:1}
      case $c in
        [a-z] | [A-Z] )
          caesar $c
          echo "  '$c' -> '$rstr'"
          ;;
        [$'\n'] )
          echo -e "  newline\n"
          ;;
        * )
          echo "  '$c' -> '$c'"
          ;;
      esac
    done
    echo
  fi
}

# Atbash cipher (reversed letters)
atbash () {  
  # If a parameter is provided to the function, it will process the provided string (for tutor function)
  if [ "$1" != "" ]; then rstr="$1"
  # Otherwise it will offset $istr
  else rstr="$istr"
  fi

  # first reverse lowercase letters
  rstr="$(tr $az $za <<< $rstr)"
  # then reverse uppercase letters
  rstr="$(tr $AZ $ZA <<< $rstr)"
}

# Atbash tutorial
atbash_tutor () {
  echo
  echo "== Atbash Cipher tutorial =="
  echo
  echo "The Atbash Cipher translates all letters in the string to their complement in the alphabet."
  echo "It is a reciprocal cipher; the same steps are used to encipher and decipher text."
  echo
  echo "To perform the Atbash cipher,"
  echo " 1. Locate each input character in the normal alphabet"
  echo " 2. Replace it with the character in the same position in the reversed alphabet:"
  echo
  # Right justify 10 spaces
  printf "%10s" "input"
  echo " = $istr"
  echo

  # Print forward and backward sequence of alphabetic characters
  echo {a..z}
  echo {z..a}
  echo

  wait_enter

  # If verbosity is over 2, we will step by step explain each word in the string
  if [[ $VERBOSE > 2 ]]; then
    # If an instr has been provided, set it to istr
    if [ "$1" != "" ]; then instr=$1
    # Otherwise set it to $istr
    else instr="$istr"
    fi

    # Using c-style loop to iterate over individual characters in $istr
    for (( i=0; i<${#istr}; i++ )); do 
      # Get one character from $istr at position $i
      c=${istr:$i:1}
      case $c in
        [a-z] | [A-Z] )
          atbash $c
          echo "  '$c' -> '$rstr'"
          ;;
        [$'\n'] )
          echo -e "  newline\n"
          ;;
        * )
          echo "  '$c' -> '$c'"
          ;;
      esac
    done
    echo 
  fi
}

# A1Z26 cipher (numbered position in alphabet)
a1z26 () {
  # Index of next letter
  idx=''
  # Resulting string
  rstr=''

  # Show tutorial explanation if verbosity 2+
  if [[ $VERBOSE > 1 ]]; then
    echo
    echo " == A1Z26 Cipher tutorial =="
    echo
    echo "The A1Z26 Cipher represents letters by their numberic position in the alphabet."
    echo "This is summarized in the name; A=1, Z=26."
    echo
    echo "To perform the A1Z26 cipher,"
    echo " 1. Locate each character or number in the input string"
    echo " 2. Replace it with the correlating item of the other type"
    echo "    a. If you are enciphering into numbers, add dashes ('-') between successive indices"
    echo "    b. If you are deciphering from numbers, remove the dashes between the indices"
    echo "    c. Pass through spaces, punctuation, other special characters"
    echo
    # Right justify 10 spaces
    printf "%10s" "input"
    echo " = $istr"
    echo

    # Print out numbered indices for the alphabet
    for n in {1..26}; do
      # %- = left justify; 3 = width; s = format string of characters
      printf "%-3s" $n
    done
    echo

    # Print out a to z to align with the numbers
    for c in {a..z}; do
      printf "%-3s" $c
    done
    echo
    echo

    wait_enter
  fi

  # Using c-style loop to iterate over individual characters in $istr
  for (( i=0; i<${#istr}; i++ )); do 
    # Get one character from $istr at position $i
    c=${istr:$i:1}
    # Original and new content added this round
    oc=''
    nc=''
    case $c in
      [a-z] | [A-Z] )
        oc+="$c"
        # If the current last character in the resulting string is a digit,
        # we will need a '-' to distinguish the different indices 
        if [[ $rstr =~ [0-9]$ ]]; then nc+='-'; fi
        # Append the index of the input char
        nc+=$(get_index $c)
        ;;
      [0-9] )
        # Add the digit character to the index variable
        idx+=$c
        ;;
      * )
        if [[ "$idx" != 0 ]]; then
          oc+="$idx"
        fi

        # A dash (or any other character) indicates the end of a numbered index.
        # Add to the resulting string the alphabet character at the current index.
        # This sometimes adds the first item in the array to the string,
        # which doesn't matter since the first item is ''
        # The old solution was: if [ $idx ]; then rstr+=${alpha[$idx-1]}; fi
        nc+=${alpha[$idx]}
        # Clear the index variable for next time
        idx=''
        # If the special character is anything other than a newline,
        # we should report it as part of the original content
        if [[ "$c" != $'\n' ]]; then
          #echo "non-newline special char"
          oc+="$c"
          # If the special character is also not a dash, should be passed on to result
          if [[ "$c" != '-' ]]; then
            nc+="$c"
          fi
        fi
        ;;
    esac

    # If step-by-step (verb 3+), output original content and new content
    if [[ $VERBOSE > 2 ]] && [[ $nc != '' ]]; then
      # add ' to start of original content for easier readability
      oc="'$oc"
      # Right justify (pad to the left) with two spaces beforehand
      printf "  %4s" "$oc"
      echo "' -> '$nc'"
    fi

    # Append new content to resulting string
    rstr+="$nc"

    # Account for newline characters separately for better step-by-step output
    if [[ "$c" == $'\n' ]]; then
      rstr+="$c"
      if [[ $VERBOSE > 2 ]]; then
        echo -e "    newline\n"
      fi
    fi
  done

  # One more append in case the last character is a digit
  rstr+=${alpha[$idx]}
  # Report extra append if step-by-step
  if [[ $VERBOSE > 2 ]]; then
    if [ $idx ]; then
      oc+="'$idx"
      nc+=${alpha[$idx]}
      printf "  %4s" "$oc"
      echo "' -> '$nc'"
    fi
    # Extra newline
    echo
  fi
}

# Vigenere cipher (offset cipher based on per-letter alphabetic index in provided key)
vigenere () {
  # working string
  wstr=''
  # resulting string
  rstr=''
  # index for key
  kin=0


  # If enciphering with the autokey method, the input string should be appended to the key
  if $AK && [[ $OP == 'en' ]]; then VKEY+="$istr"; fi
  # Remove spaces from key (may be added from $istr)
  VKEY="$(echo $VKEY | tr -d ' ')"
  
  # Show tutorial explanation if verbosity 2+
  if [[ $VERBOSE > 1 ]]; then
    echo
    echo " == Vigenere Cipher tutorial =="
    echo
    echo "The Vigenere Cipher is a more elaborate Caesar cipher in which the offset varies per letter."
    echo "The key provided to the cipher determines this per-letter offset."
    if $AK; then
      echo
      echo "The Autokey variant has been selected, so the plain text should be appended to the key."
      if [[ $OP == 'en' ]]; then
        echo "It is currently set to *encipher*, so the input text can be immediately appended to the key."
      else
        echo "It is currently set to *decipher*, so the plain text must be appended to the key as it is deciphered."
      fi
    fi
    echo
    echo "To perform the Vigenere cipher,"
    echo " 1. Line each alphabetic input character with the corresponding alpha or numeric character in the key"
    echo "    a. Pass through all non-alpha input characters"
    echo "    b. Repeat the key as needed"
    echo " 2. Locate the key character's index in the alphabet (or literal value if a digit)"
    echo " 3. Replace the input character with the character that many characters to the $DIRECTION"
    if $AK && [[ $OP == 'de' ]]; then
      echo "    a. (Autokey) Add the new character to the end of the key"
    fi
    echo
    # Right justify 10 spaces
    printf "%10s" "input"
    echo " = $istr"
    printf "%10s" "key"
    echo " = $VKEY"
    printf "%10s" "operation"
    echo " = ${OP}cipher $DIRECTION"
    echo

    # Print out numbered indices for the alphabet (like A1Z26)
    for n in {1..26}; do
      printf "%-3s" $n
    done
    echo

    # Print out a to z to align with the numbers
    for c in {a..z}; do
      printf "%-3s" $c
    done
    echo; echo

    wait_enter
  fi

  #process_start=$(now)

  # using c-style loop to iterate over individual characters in $istr
  for (( i=0; i<${#istr}; i++ )); do 
    # length of the key
    # this is recalculated each loop for compatibility with deciphering autokey
    klen=${#VKEY}
    # Get one character from $istr at position $i
    c=${istr:$i:1}
    # Check if the character is a letter
    if [[ $c =~ ^[[:alpha:]]$ ]]; then
      # Find the corresponding offset character using modulo so it wraps around
      # when $istr is longer than $VKEY, which is a common occurrence (unless autokey)
      k=${VKEY:$(($kin % $klen)):1}
      # Assign the $DIST varaible for the offset function to the index of the key char
      # minus one since A should match with 0 offset
      DIST=$(( $(get_index $k) - 1 ))
      # Perform offset only on the current char from $istr
      caesar $c
      # Internally, the Caesar function uses $rstr, so this function uses $wstr until the end
      wstr+=$rstr
      # If deciphering with the autokey method, append deciphered alphanumeric characters to the key
      if $AK && [[ $OP == 'de' ]]; then VKEY+=$rstr; fi
      # Increment key index
      (( kin++ ))

      # Step-by-step commentary
      if [[ $VERBOSE > 2 ]]; then
        echo "  '$c' -> '$rstr' ( $k = $DIST )"
      fi
    else
      # Key characters should only match up with alpha characters, 
      # so kin is only incremented when encountering alpha in the input (previous branch)
      # Here, we just append whatever other char we have to wstr
      wstr+=$c

      if [[ $VERBOSE > 2 ]]; then
        if [[ "$c" == $'\n' ]]; then
          echo -e "  newline\n"
        else
          echo "  '$c' -> '$c'"
        fi
      fi
    fi
  done

  #runtime "$process_start" "processing Vigenere cipher"

  if [[ $VERBOSE > 2 ]]; then echo; fi

  # Store the working string into the resulting string for later printing
  rstr="$wstr"
}

# Beaufort cipher (symmetric substitution cipher similar to the Vigenere cipher)
beaufort () {
  # Resulting string
  rstr=''

  # index for key
  kin=0
  # length of the key
  klen=${#BKEY}

  # Remove spaces from key
  BKEY="$(echo $BKEY | tr -d ' ')"

  # Show tutorial explanation if verbosity 2+
  if [[ $VERBOSE > 1 ]]; then
    echo
    echo " == Beaufort Cipher tutorial =="
    echo
    echo "The Beaufort cipher is similar to the Vigenere cipher in that it processes"
    echo "each input character differently based on the provided key."
    echo "It is different in that it is a *reciprocal* cipher,"
    echo "so the same procedure is used to encipher and decipher text."
    echo
    echo "To perform the Beaufort cipher:"
    echo " 1. Line up each alphabetic input character with the corresponding alpha or numeric character in the key"
    echo "    a. Pass through all non-alpha input characters"
    echo "    b. Repeat the key as needed"
    echo " 2. Find the alphabetic indexes for both the input and key characters"
    echo " 3. The resulting character is the character *input* spaces to the L from the *key* character"
    echo "    a. To summarize, result = key - input"
    echo "    b. Refer to the positive or negative indices listed below depending on the result"
    echo
    # Right justify 10 spaces
    printf "%10s" "input"
    echo " = $istr"
    printf "%10s" "key"
    echo " = $BKEY"
    printf "%10s" "formula"
    echo " = result = key - input"
    echo

    # Print out positive indices for the alphabet starting at 0
    for n in {0..25}; do
      printf "%-3s" $n
    done
    echo " - positive indices"

    # Print out a to z to align with the numbers
    for c in {a..z}; do
      printf "%-3s" $c
    done
    echo

    # Print out negative indices for the alphabet
    for n in {26..1}; do
      printf "%-3s" $n
    done
    echo " - negative indices"
    echo

    wait_enter
  fi

  # using c-style loop to iterate over individual characters in $istr
  for (( i=0; i<${#istr}; i++ )); do 
    # Get one character from $istr at position $i
    c=${istr:$i:1}
    # If the character is a letter
    if [[ $c =~ ^[[:alpha:]]$ ]]; then
      # Find the corresponding key character using modulo so it wraps around
      # when $istr is longer than $BKEY, which is a common occurrence
      k=${BKEY:$(($kin % $klen)):1}
      # Find alphabetic indices for key and input character
      aik=$(get_index $k)
      aic=$(get_index $c)
      # The index of the result character is 
      # the index of the key character minus the index of the input character
      idx=$(( $aik - $aic ))
      # Add the result character to the result string
      nc=${alpha[$idx]}
      rstr+=$nc
      # Increment key index
      (( kin++ ))

      # Step-by-step commentary
      if [[ $VERBOSE > 2 ]]; then
        echo "  '$c' -> '$nc' -- ( $k=$aik ) - ( $c=$aic ) = ( $idx=$nc )"
      fi
    else
      # Key characters should only match up with alpha characters, 
      # so kin is only incremented when encountering alpha in the input (previous branch)
      # Here, we just append whatever other char we have to wstr
      rstr+="$c"

      if [[ $VERBOSE > 2 ]]; then
        if [[ "$c" == $'\n' ]]; then
          echo -e "  newline\n"
        else
          echo "  '$c' -> '$c'"
        fi
      fi
    fi
  done
  
  if [[ $VERBOSE > 2 ]]; then echo; fi
}

# Keyword cipher (monoalphabetic substitution cipher determined by a keyword)
keyword () {
  # If a parameter is provided to the function, it will process the provided string (for tutor function)
  if [ "$1" != "" ]; then rstr="$1"
  # Otherwise it will offset $istr
  else rstr="$istr"
  fi

  # Construct upper and lowercase cipher alphabet strings (Key-to-Z)
  kz="$(echo ${alpha[@]} | tr -d ' ')"
  KZ="${kz^^}"

  # The actual operation depends on whether we're enciphering or deciphering
  if [[ $OP == 'de' ]]; then
    # To decipher, translate k-z and K-Z back to a-z and A-Z
    rstr="$(tr $kz $az <<< $rstr)"
    rstr="$(tr $KZ $AZ <<< $rstr)"
  else
    # To encipher (default), translate a-z and A-Z to k-z and K-Z
    rstr="$(tr $az $kz <<< $rstr)"
    rstr="$(tr $AZ $KZ <<< $rstr)"
  fi
}

# Mixed alphabet tutorial, used by Keyword, Playfair, and Bifid
malpha_tutor () {
  KEY="$1"
  echo "To construct the mixed alphabet:"
  echo " 1. Write out all ${bl}unique${nm} letters in the provided key: ${bl}$KEY${nm}"
  echo "    a. Only the ${bl}first occurrence${nm} of duplicate letters should remain"
  echo " 2. After the key, write out all the remaining letters of the alphabet in their regular order"
  echo
  echo "You may prefer to think of it in the way that the script accomplishes it:"
  echo " 1. Start with the normal alphabet"
  echo " 2. For each ${bl}unique${nm} letter in the provided key:"
  echo "    a. Move it to just before the first letter in the ${bl}untouched${nm} alphabet"
  echo
  printf "%10s" "key"
  echo " = $KEY"
  echo

  wait_enter "Construct the alphabet"
}

# Keyword tutorial
keyword_tutor () {
  echo
  echo " == Keyword Cipher tutorial =="
  echo
  echo "The Keyword cipher is a monoalphabetic substitution cipher that"
  echo "uses the provided key to construct a mixed cipher alphabet."
  echo
  malpha_tutor "$KKEY"
  echo "Now that we have the mixed alphabet constructed:"
  if [[ $OP == 'de' ]]; then
    echo " 1. Locate each input character in the mixed alphabet"
    echo " 2. Replace it with the character in the same position in the normal alphabet"
  else
    echo " 1. Locate each input character in the normal alphabet"
    echo " 2. Replace it with the character in the sameposition in the mixed alphabet"
  fi
  echo
  # Right justify 10 spaces
  printf "%10s" "input"
  echo " = $istr"
  echo

  # Print normal and mixed sequence of alphabetic characters
  echo {a..z}

  for c in ${alpha[@]}; do 
    echo -n "$c "
  done
  echo; echo

  wait_enter

  # If verbosity is over 2, we will step by step explain each word in the string
  if [[ $VERBOSE > 2 ]]; then
    # If an instr has been provided, set it to istr
    if [ "$1" != "" ]; then instr=$1
    # Otherwise set it to $istr
    else instr="$istr"
    fi

    # Using c-style loop to iterate over individual characters in $istr
    for (( i=0; i<${#istr}; i++ )); do 
      # Get one character from $istr at position $i
      c=${istr:$i:1}
      case $c in
        [a-z] | [A-Z] )
          keyword $c
          echo "  '$c' -> '$rstr'"
          ;;
        [$'\n'] )
          echo -e "  newline\n"
          ;;
        * )
          echo "  '$c' -> '$c'"
          ;;
      esac
    done
    echo 
  fi
}

# Polybius square tutorial, used by Playfair and Bifid
psquare_tutor () {
  echo "To construct the Polybius square in a 5x5 grid:"
  echo " 1. Remove the letter '${bl}j${nm}' from the alphabet"
  echo "    a. We only have 25 total spaces in the square"
  echo "    b. Instances of '${bl}j${nm}' will be replaced with '${bl}i${nm}'"
  echo " 2. Place the ${bl}first 5${nm} characters in the alphabet on the first row"
  echo " 3. Place the ${bl}next 5${nm} characters on the second row and ${bl}repeat${nm} for the other 3 rows"
  echo
  # Print mixed alphabet with spaces between letters
  for c in ${alpha[@]}; do 
    echo -n "$c "
  done
  echo; echo

  wait_enter "Construct the square"
}

# Playfair cipher (digram substitution cipher using Polybius square)
playfair () {
  # Playfair considers 'j' as 'i' and doesn't like spaces
  # Store substituted string in $wstr
  wstr="$(tr j i <<< ${istr//[[:space:]]/})"
  # Resulting string
  rstr=''

  if [[ $VERBOSE > 1 ]]; then
    echo
    echo " == Playfair Cipher tutorial =="
    echo
    echo "The Playfair cipher is a digram substitution cipher that uses"
    echo "a mixed alphabet and Polybius square to process two characters at once"
    echo
    malpha_tutor "$PKEY"
    psquare_tutor
    echo "Now that we have the Polybius square, we can perform the Playfair cipher:"
    echo " 1. Replace all j's in the input string with i"
    echo " 2. Loop through each pair of letters in the input string"
    echo "    a. If you encounter a pair of the same letter, add 'x' in between"
    echo "       ('falls' -> 'fa lx ls')"
    echo "    b. If you reach the end of the string with just one letter, add an extra 'x'"
    echo "       ('welcome' -> 'we lc om ex')"
    echo " 3. Locate the pair of letters in the Polybius square and find which rule it matches:"
    if [[ "$OP" == "de" ]]; then
      echo "    a. If they appear in the same colun, replace each letter with the one immediately above it"
      echo "       ('$(get2d 2 2)$(get2d 2 3)' -> '$(get2d 2 1)$(get2d 2 2)')"
      echo "    b. If they appear in the same row, replace each letter with the one immediately to the left"
      echo "       ('$(get2d 4 2)$(get2d 0 2)' -> '$(get2d 3 2)$(get2d 4 2)')"
    else
      echo "    a. If they appear in the same colun, replace each letter with the one immediately below it"
      echo "       ('$(get2d 2 1)$(get2d 2 2)' -> '$(get2d 2 2)$(get2d 2 3)')"
      echo "    b. If they appear in the same row, replace each letter with the one immediately to the right"
      echo "       ('$(get2d 3 2)$(get2d 4 2)' -> '$(get2d 4 2)$(get2d 0 2)')"
    fi
    echo "    c. If neither of those match, imagine a rectangle around them and select the other corners of it"
    echo "       Each letter should be replaced with the other corner that is on the same row"
    echo "       ('$(get2d 4 2)$(get2d 0 1)' -> '$(get2d 0 2)$(get2d 4 1)')"
    echo " 4. (opt.) The script doesn't do this because it is difficult to do programatically,"
    echo "    but you may wish to add spaces and special characters back in where they make sense"
    echo
    # Right justify 10 spaces
    printf "%10s" "input"
    echo " = $istr"
    echo

    # Print out 2d Polybius square
    print2d
    echo

    wait_enter
  fi

  # Using c-style loop to iterate over individual characters in $wstr
  for (( i=0; i<${#wstr}; i++ )); do 
    # Get current and next letters in $wstr
    cur=${wstr:$i:1}
    nxt=${wstr:$((i+1)):1}

    if [[ $cur =~ ^[[:alpha:]]$ ]]; then
      # If the next value is empty, it's the end of the string
      if [[ -z "$nxt" ]]; then
        # Pad with 'x'
        nxt=x
      # If the two characters are the same
      elif [[ "$cur" == "$nxt" ]]; then
        # Pad with 'x'
        nxt=x
      # If the next value is another alpha character, we will skip it next loop
      elif [[ $nxt =~ ^[[:alpha:]]$ ]]; then
        (( i++ ))
      else
        # If not a letter, we need to delete it from the string
        # Using sed at $i + 2 since 1 would be the first character
        # and we need to delete the next one
        wstr="$(echo $wstr | sed "s/.//$(( $i + 2 ))")"
        # Decrement i to attempt this pair again after deleting the character
        (( i-- ))
        # Continue with the loop
        continue
      fi

      # Get current and next indices
      ci=$(get_index $cur)
      ni=$(get_index $nxt)

      # Check for errors
      # If either is empty, return error and continue with loop
      if [[ "$ci" == "" ]]; then
        echo "Couldn't get index for $cur" > /dev/stderr
        continue
      elif [[ "$ni" == "" ]]; then
        echo "Couldn't get index for $nxt" > /dev/stderr
        continue
      fi

      # Get current and next x/y position
      # Observe the layout of print2d() if this doesn't make sense
      cx=$(( $ci % 5 ))
      cy=$(( $ci / 5 ))
      nx=$(( $ni % 5 ))
      ny=$(( $ni / 5 ))

      # New content added this loop run
      nc=''

      # If they're in the same "column"
      if [[ $cx == $nx ]]; then
        # Each character is "up" one row for deciphering (down for enciphering)
        if [[ "$OP" == "de" ]]; then
          nc+="$(get2d $cx $(( $cy - 1 )))$(get2d $nx $(( $ny - 1 )))"
        else
          nc+="$(get2d $cx $(( $cy + 1 )))$(get2d $nx $(( $ny + 1 )))"
        fi
      # If they're in the same "row"
      elif [[ $cy == $ny ]]; then
        # Each character is to the left one column for deciphering (right for enciphering)
        if [[ "$OP" == "de" ]]; then
          nc+="$(get2d $(( $cx - 1 )) $cy)$(get2d $(( $nx - 1 )) $ny)"
        else
          nc+="$(get2d $(( $cx + 1 )) $cy)$(get2d $(( $nx + 1 )) $ny)"
        fi
      # If neither x nor y match between them
      else
        # Swap x-values, same for (en/de)ciphering
        nc+="$(get2d $nx $cy)$(get2d $cx $ny)"
      fi

      rstr+="$nc "

      # Step-by-step commentary if sufficiently verbose
      if [[ $VERBOSE > 2 ]]; then
        echo "  '$cur$nxt' -> '$nc'"
      fi
    else
      # If not a letter, we need to delete it from the string
      # Using sed at $i + 1 since 1 would be the first character 
      wstr="$(echo $wstr | sed "s/.//$(( $i + 1 ))")"
      # Decrement i to attempt this pair again after deleting the character
      (( i-- ))
    fi
  done

  if [[ $VERBOSE > 2 ]]; then echo; fi
}

# Bifid cipher (digraphic cipher using Polybius square like Playfair)
bifid () {
  # Bifid (like Playfair) considers 'j' as 'i'
  # Store substituted string in $wstr
  wstr="$(tr jJ iI <<< $istr)"
  # Resulting string
  rstr=''

  if [[ $VERBOSE > 1 ]]; then
    echo
    echo " == Bifid Cipher tutorial =="
    echo
    echo "The Bifid cipher is another somewhat complicated cipher that uses"
    echo "a mixed alphabet and Polybius square."
    echo "The cipher then intersperses ${bl}row${nm} (Y) and ${bl}column${nm} (X) values to mask the contents."
    echo
    malpha_tutor "$IKEY"
    psquare_tutor
    echo "Now that we have the Polybius square, we can start with the cipher by collecting row/column values:"
    echo " 1. For each letter in the input string, locate it in the Polybius square"
    if [[ "$OP" == "de" ]]; then
      echo " 2. Add the row and column values to a ${bl}single list${nm} of coordinates"
    else
      echo " 2. Add the row and column values to ${bl}separate lists${nm}"
    fi
    echo
    # Right justify 10 spaces
    printf "%10s" "input"
    echo " = $istr"
    echo

    # Print out 2d Polybius square
    print2d
    echo

    wait_enter "Create the list(s)"
  fi

  # Empty arrays for X and Y values
  arrx=( )
  arry=( )
  # Full array for y-values interspersed with x-values
  fullarr=( )

  # Using c-style loop to iterate over individual characters in $wstr
  for (( i=0; i<${#wstr}; i++ )); do 
    # Get current letter in wstr
    cur=${wstr:$i:1}

    # If it's an alphabetic character, get the index
    if [[ $cur =~ ^[[:alpha:]]$ ]]; then
      ci=$(get_index $cur)
      # Get current x/y position
      # Observe the layout of print2d() if this doesn't make sense
      cx=$(( $ci % 5 ))
      cy=$(( $ci / 5 ))
      # Append to arrays depending on operation
      if [[ "$OP" == "de" ]]; then
        fullarr+=( $cy $cx )
      else
        arrx+=( $cx )
        arry+=( $cy )
      fi
    fi
  done

  if [[ "$OP" == "de" ]]; then
    arrlen=${#fullarr[@]}
    # Check and report potential problem if the full array length is odd
    if (( $arrlen % 2 )); then
      echo "bifid(): full array has an odd number of items; result may be incorrect" > /dev/stderr
    fi

    if [[ $VERBOSE > 1 ]]; then
      echo "With the full list of coordinates, we need to split them up into row and column values:"
      echo " 1. Add the ${bl}first $PERIOD${nm} (current period value) numbers to the list of row values"
      echo " 2. Add the ${bl}next $PERIOD${nm} numbers to the list of column values"
      echo " 3. Repeat until you have less than ${bl}$(( $PERIOD * 2 ))${nm} values left"
      echo " 4. Add the ${bl}first half${nm} of the remaining numbers to the list of row values,"
      echo "    the ${bl}second half${nm} to the list of column values"
      echo
      
      # Right justify 10 spaces
      printf "%10s" "period"
      echo " = $PERIOD"
      printf "%10s" "coords"
      echo " = ${fullarr[@]}"
      echo

      wait_enter "Split the list"
    fi

    # Copy $PERIOD to another variable (interval) so we can edit it if needed
    # to account for string lengths that aren't a multiple of $PERIOD
    ivl=$PERIOD
    # Loop through every other multiple of $PERIOD;
    for i in $(seq 0 $(( $PERIOD * 2 )) $arrlen); do
      # Each run processes the next $PERIOD * 2 items.
      # We need to check if this will place it past the end of the array.
      # $diff is the number of items it will want to process this run
      # that do NOT have array elements corresponding to them.
      diff=$(( ($i + ($PERIOD * 2) - $arrlen) ))
      # If it is negative or zero, we're good.
      if (( $diff > 0 )); then
        # If it is positive, we need to alter the behavior so that
        # half of the remaining array elements are added as y- and x-values each.
        # For this run, the interval on which it operates needs to be
        # $diff / 2 less than the typical $PERIOD interval.
        ivl=$(( $PERIOD - ($diff / 2) ))
      fi
      # Add the next $ivl items as y-values (rows)
      arry+=( ${fullarr[@]:$i:$ivl} )
      # Add the next $ivl items after that as x-values (columns)
      arrx+=( ${fullarr[@]:$(( $i + $ivl )):$ivl} )
    done

    if [[ $VERBOSE > 1 ]]; then
      echo "The two lists can now be read directly as row/column values for the result."
      echo
      printf "%10s" "rows"
      echo " = ${arry[@]}"
      printf "%10s" "columns"
      echo " = ${arrx[@]}"
      echo
    fi
  else
    if [[ $VERBOSE > 1 ]]; then
      echo "With the lists of row and column values, we need to unity them into one list:"
      echo " 1. Add the first $PERIOD (current period value) ${bl}row${nm} values to the full list"
      echo " 2. Add the next $PERIOD ${bl}column${nm} values to the full list"
      echo " 3. ${bl}Repeat${nm} until all values have been added to the full list"
      echo
      # Right justify 10 spaces
      printf "%10s" "period"
      echo " = $PERIOD"
      printf "%10s" "rows"
      echo " = ${arry[@]}"
      printf "%10s" "columns"
      echo " = ${arrx[@]}"
      echo

      wait_enter "Create the full list"
    fi

    # Loop through values from 0 to the x array length in $PERIOD increments
    for i in $(seq 0 $PERIOD ${#arrx[@]}); do
      # Full array should be $PERIOD y-values, then that many x-values,
      # then the next $PERIOD y- then x-values until arry/arrx are exhausted
      fullarr+=( ${arry[@]:$i:$PERIOD} ${arrx[@]:$i:$PERIOD} )
    done

    if [[ $VERBOSE > 1 ]]; then
      echo "The full list that we now have is the list of row and column values for the result."
      echo "For example, the first result character is at row=${fullarr[0]}, col=${fullarr[1]}"
      echo
      printf "%10s" "coords"
      echo " = ${fullarr[@]}"
      echo
    fi
  fi

  if [[ $VERBOSE > 1 ]]; then
    echo "You may also want to refer back to the input string and add back"
    echo "spaces and special characters in the same positions in the resulting string."
    echo
    # Right justify 10 spaces
    printf "%10s" "input"
    echo " = $istr"
    echo

    # Print out 2d Polybius square
    print2d
    echo

    wait_enter
  fi

  # Array index
  ai=0

  # Looping through the original string again to simplify spaces/special chars
  for (( i=0; i<${#wstr}; i++ )); do
    # Get current letter from $wstr
    cur=${wstr:$i:1}

    nc=''
    # If it's an alphabetic character, add substitution character to result
    if [[ $cur =~ ^[[:alpha:]]$ ]]; then
      if [[ "$OP" == "de" ]]; then
        # Get x/y values depending on operation
        cy=${arry[$ai]}
        cx=${arrx[$ai]}
        (( ai++ ))
      else
        # The x,y values for enciphering are the next two items in the full array
        cy=${fullarr[$ai]}
        cx=${fullarr[$(( ai + 1 ))]}
        # Increment index variable by 2
        (( ai+=2 ))
      fi
      nc=$(get2d $cx $cy)
      if [[ $VERBOSE > 2 ]]; then
        echo "  '$cur' -> '$nc' (r=$cy, c=$cx)"
      fi
    else
      # If not an alphabetic char, add whatever else it is to the result
      nc="$cur"
      if [[ $VERBOSE > 2 ]]; then
        if [[ "$cur" == $'\n' ]]; then
          echo -e "  newline\n"
        else
          echo "  '$cur' -> '$nc'"
        fi
      fi
    fi
    rstr+="$nc"
  done

  if [[ $VERBOSE > 2 ]]; then echo; fi
}

# Customized monoalphabetic substitution
substitute () {
  # Initialize wstr
  wstr="$istr"

  # Normal and substitution strings
  az=''
  sz=''
  # Variable for how to handle case for the substitution function
  # This determines the case of SUBSTITUTED letters; untouched letters will be the opposite case
  # Default to capitalizing input and replacing affected characters with lowercase
  subcase=C

  # Iterate over the substitution directives
  # Bash for loops do not recognize changes to the array while the loop is in progress.
  # So, unlike Python, we use two separate loops, 
  # once to process file and case directives,
  # and another to process substitution directives.
  # This will have some effects on edge cases detailed in the wiki.
  for arg in ${SUBARGS[@]}; do
    if [[ $arg =~ ^file= ]]; then
      # Grab filename as portion of argument after = sign
      fname=$(echo $arg | cut -d= -f2)
      if [[ $VERBOSE > 1 ]]; then echo "Will read from file: $fname"; fi

      # Read lines from the provided filename and add to SUBARGS
      # This will error but continue if the filename isn't valid
      while read l; do
        SUBARGS+=("$l")
      done < $fname
    elif [[ $arg =~ ^case= ]]; then
      # Attempt to canonicalize the provided case designator
      if ! subcase=$(canoncase $(echo $arg | cut -d= -f2)); then
        echo "Will use the default case" > /dev/stderr
      fi
    elif [[ $arg =~ ^(src|from)= ]]; then
      newsrc=$(echo $arg | cut -d= -f2)
      az+=$newsrc
    elif [[ $arg =~ ^(dst|to)= ]]; then
      newdst=$(echo $arg | cut -d= -f2)
      sz+=$newdst
    fi
  done

  # Second time through the loop
  for arg in ${SUBARGS[@]}; do
    # Ignore any special directives on this pass
    if [[ $arg =~ ^(file|case|src|dst|from|to)= ]]; then
      continue
    # Verify that the first two characters are letters
    elif [[ $arg =~ ^[a-zA-Z]{2} ]]; then
      # Grab the first and second letters of the input
      # and add to the normal and substitution lists, respectively
      az+=${arg:0:1}
      sz+=${arg:1:1}
      if [[ $VERBOSE > 1 ]]; then echo "Will substitute $arg"; fi
    else
      echo "Ignoring unintelligible argument: $arg" > /dev/stderr
    fi
  done

  # Compare lengths of the substitution strings
  # 'tr' in bash is fine with non-matching lengths,
  # but Python's translate isn't so it's truncated in both for consistency.
  if [[ ${#az} != ${#sz} ]]; then
    echo "Source and destination sets are not the same length;" > /dev/stderr
    echo "will truncate to the shorter length" > /dev/stderr
    az=${az:0:${#sz}}
    sz=${sz:0:${#az}}
  fi

  # Initialize various cases of the original and substitution lists
  azAZ=${az,,}${az^^}
  szsz=${sz,,}${sz,,}
  szSZ=${sz,,}${sz^^}
  SZSZ=${sz^^}${sz^^}
  # Variable for the selected sz version
  selsz=''

  # If subcase is none, then we will translate preserving case
  # This is NOT the default as it obfuscates which parts of the string were translated
  # and which still need analyzed to attempt a substitution
  if [[ $subcase == "N" ]]; then
    if [[ $VERBOSE > 1 ]]; then echo "Preserving original case"; fi
    selsz=$szSZ
  # If subcase is capital, then the input should be lowercase
  # and all substitutions should be uppercase
  elif [[ $subcase == "C" ]]; then
    if [[ $VERBOSE > 1 ]]; then echo "Replacements will be in uppercase"; fi
    wstr=${istr,,}
    selsz=$SZSZ
  # If subcase is capital then the input should be uppercase
  # and all substitutions should be lowercase
  elif [[ $subcase == "S" ]]; then
    if [[ $VERBOSE > 1 ]]; then echo "Replacements will be in lowercase"; fi
    wstr=${istr^^}
    selsz=$szsz
  fi

  # Perform substitution
  if ! rstr="$(tr $azAZ $selsz <<< $wstr)"; then
    echo "Unable to perform substitution; probably had no substitution directives." > /dev/stderr
    usage noarg
  fi
}

# Generate a random monoalphabetic cipher operation
randomize () {
  rstr="$istr"

  # This will be the substitution alphabet
  rz=''

  # If the Caesar flag was provided, do a random Caesar offset
  if $CAE; then
    if [[ $VERBOSE > 1 ]]; then echo "Generating random Caesar offset"; fi
    # Generate random number between 1 and 25
    DIST=$((1 + $RANDOM % 25))
    # Always go right (although direction doesn't reall matter here)
    DIRECTION=R
    # Perform Caesar operation
    caesar
    # Set rz so the report knows what cipher operation was done
    rz="Caesar $DIST"
  # If no other recognized flags provided, do a fully random monoalphabetic substitution
  else
    if [[ $VERBOSE > 1 ]]; then echo "Generating random monoalphabetic substitution"; fi
    # Randomize normal alphabet list
    ralpha=( $(shuf -e "${alpha[@]}") )

    # Loop through regular alphabet to check for non-substituted letters.
    # The `shuf` command doesn't guarantee that every item will move from its original position.
    # However, for our purposes, we would like each alphabetic letter to
    # be substituted for a different letter.
    # This will accomplish that with one rare limitation:
    # if 'z' is not moved this will not be able to correct it.
    for c in ${alpha[@]}; do
      # Substitution character should be the first item in the
      # remaining randomized alphabet array
      sc=${ralpha[0]}
      # If the substitution letter is the same and there are enough available random letters,
      if [[ $c == $sc ]] && [[ ${#ralpha[@]} > 1 ]]; then
        if [[ $VERBOSE > 2 ]]; then echo "Same letter, looking ahead"; fi
        # use the next item in the array as the substitution letter instead
        sc=${ralpha[1]}
      fi
      # Add the substitution letter to the substitution alphabet string
      rz+=$sc
      if [[ $VERBOSE > 2 ]]; then echo "$c -> $sc"; fi
      # Remove the substitution character from the array
      ralpha=( ${ralpha[@]/$sc} )
    done

    if [[ $VERBOSE > 1 ]]; then
      echo "Finalized substitution alphabet: $rz"
    fi

    # Create uppercase substitution string
    RZ="${rz^^}"
    
    # Use tr to replace the randomized subsittution alphabet
    # in both lower and upper case.
    rstr="$(tr $az $rz <<< $rstr)"
    rstr="$(tr $AZ $RZ <<< $rstr)"
  fi
}

# Report current state of the resulting string, stripping spaces if requested
report () {
  if $STRIP; then
    echo "$rstr" | tr $SET1 $SET2 | tr -d [[:blank:]]
  else
    echo "$rstr" | tr $SET1 $SET2
  fi
}

########## End of function declarations

########## Start of execution

#setup_start=$(now)
# initialize basic variables
init_vars

# Pass all command line arguments to get_args() for parsing
get_args "$@"

# Initialize strings
init_str
alphinit

# Determine contents of input string from the contains() function
CONT=$(contains)

#runtime $setup_start "script setup"

# Perform each cipher based on the contents of the input
# or confirmation to continue anyway

#ciphers_start=$(now)

# Random cipher substitution
if $RAND; then
  randomize
  if [[ $VERBOSE > 0 ]]; then echo "  Processed with ${bl}random monoalphabetic substitution${nm} ($rz):"
  fi
  report
  exit
fi

# Caesar
if $CAE; then
  # If the input contains letters, we're good. Otherwise we need to confirm
  if [ $CONT = "letters" ] || confirm; then 
    # Use Caesar Tutor if verbosity is 2+
    if [[ $VERBOSE > 1 ]]; then caesar_tutor; fi
    caesar
    # If brute force is set ('-b'), then output is provided in the function
    if ! $BR; then
      # Provide details about the function(s) run if verbose
      if [[ $VERBOSE > 0 ]]; then echo "  Processed with ${bl}Caesar $DIRECTION $DIST${nm}:"; fi
      # Report the resulting string
      report
    fi
  fi
fi

# Atbash
if $ATB; then
  if [ $CONT = "letters" ] || confirm; then 
    if [[ $VERBOSE > 1 ]]; then atbash_tutor; fi
    atbash
    if [[ $VERBOSE > 0 ]]; then echo "  Processed with ${bl}Atbash${nm}:"; fi
    report
  fi
fi

# A1Z26
if $A1Z; then
  # No input validation as it will encipher letters and decipher numbers
  alphinit
  a1z26
  if [[ $VERBOSE > 0 ]]; then echo "  Processed with ${bl}A1Z26${nm}:"; fi
  report
fi

# Vigenere
if $VIG; then
  # Vigenere (and several other ciphers) requires a key
  # Validate and error out if none provided
  if [ "$VKEY" = "" ]; then
    echo "Need to specify key for Vigenere cipher." > /dev/stderr
    usage noarg
  fi

  if [ $CONT = "letters" ] || confirm; then 
    # Vigenere cipher should not be run with brute force mode
    # Running with BR would display each offset for each letter
    BR=false
    vigenere
    if [[ $VERBOSE > 0 ]]; then echo "  Processed with ${bl}Vigenere $DIRECTION $VKEY${nm}:"; fi
    report
  fi
fi

# Beaufort
if $BEA; then
  if [ "$BKEY" = "" ]; then
    echo "Need to specify key for Beaufort cipher." > /dev/stderr
    usage noarg
  fi

  if [ $CONT = "letters" ] || confirm; then 
    alphinit b
    beaufort
    if [[ $VERBOSE > 0 ]]; then echo "  Processed with ${bl}Beaufort $BKEY${nm}:"; fi
    report
  fi
fi

# Keyword
if $KW; then
  if [ "$KKEY" = "" ]; then
    echo "Need to specify key for Keyword cipher." > /dev/stderr
    usage noarg
  fi

  if [ $CONT = "letters" ] || confirm; then 
    alphinit k
    if [[ $VERBOSE > 1 ]]; then keyword_tutor; fi
    keyword
    if [[ $VERBOSE > 0 ]]; then echo "  Processed with ${bl}Keyword $KKEY${nm}:"; fi
    report
  fi
fi

# Playfair
if $PF; then
  if [ "$PKEY" = "" ]; then
    echo "Need to specify key for Playfair cipher." > /dev/stderr
    usage noarg
  fi

  if [ $CONT = "letters" ] || confirm; then 
    alphinit p
    playfair
    if [[ $VERBOSE > 0 ]]; then echo "  Processed with ${bl}Playfair $PKEY${nm}:"; fi
    report
  fi
fi

# Bifid
if $BIF; then
  if [ "$IKEY" = "" ]; then
    echo "Need to specify key for Bifid cipher." > /dev/stderr
    usage noarg
  fi

  if [ $CONT = "letters" ] || confirm; then 
    alphinit i
    bifid
    if [[ $VERBOSE > 0 ]]; then echo "  Processed with ${bl}Bifid $IKEY $PERIOD${nm}:"; fi
    report
  fi
fi

# Custom substitution
if $SUB; then
  # Verify that we've gotten some arguments describing how to substitute
  if [ ${#SUBARGS[@]} -eq 0 ]; then
    echo "Need at least one argument for custom substitutions." > /dev/stderr
    echo "Got ${SUBARGS[@]}" > /dev/stderr
    usage noarg
  fi

  if [ $CONT = "letters" ] || confirm; then 
    if [[ $VERBOSE > 1 ]]; then echo ${SUBARGS[@]}; fi
    substitute
    if [[ $VERBOSE > 0 ]]; then echo "  Processed with custom monoalphabetic substitution:"; fi
    report
  fi
fi

#runtime $ciphers_start "cipher operation(s)"
