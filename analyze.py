#!/usr/bin/python3

# Ciphertext analysis script

import argparse, os, sys, string, platform

from collections import defaultdict
from math import isclose
from itertools import zip_longest

# Simple class for color codes
class color:
  bl = '\033[1m'
  nm = '\033[0m'

# Simple class for margins used by closeenough
class margins:
  coarse = 0.6
  precise = 0.4

# Typical frequencies of English letters
# from http://pi.math.cornell.edu/~mec/2003-2004/cryptography/subs/frequencies.html
engfreq = {
  'a': 8.12,
  'b': 1.49,
  'c': 2.71,
  'd': 4.32,
  'e': 12.02,
  'f': 2.30,
  'g': 2.03,
  'h': 5.92,
  'i': 7.31,
  'j': 0.10,
  'k': 0.69,
  'l': 3.98,
  'm': 2.61,
  'n': 6.95,
  'o': 7.68,
  'p': 1.82,
  'q': 0.11,
  'r': 6.02,
  's': 6.28,
  't': 9.10,
  'u': 2.88,
  'v': 1.11,
  'w': 2.09,
  'x': 0.17,
  'y': 2.11,
  'z': 0.07
}

# List of lowecase letters in alphabet
alpha = list(string.ascii_lowercase)

# Most common pairs of letters in English
# from https://www.simonsingh.net/The_Black_Chamber/hintsandtips.html
engpairs = ["th", "er", "on", "an", "re", "he", "in", "ed", "nd", "ha", "at", "en", "es", "of",
    "or", "nt", "ea", "ti", "to", "it", "st", "io", "le", "is", "ou", "ar", "as", "de", "rt", "ve"]
# And pairs of the same letter, same source
engdoubles = ["ss", "ee", "tt", "ff", "ll", "mm", "oo"]

##### Supporting functions

# Function to get args for more easy navigation of the code
def get_args():
  # Initialize parser 
  # No built-in help so we can add it separately to the "Script modifiers" group
  # Using alternate formatter so the longer description can be used
  # Setting description and epilog for usage synopsis
  parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter, 
  description='''
  Cipher Analysis script
  Written by Stephen Kendall

  Provides useful analysis information for a ciphertext-only attack.
  So far, intended for use with monoalphabetic subsitution ciphers.''', 
  epilog="Consult the Wiki for more details: https://gitlab.com/SirDoctorK/cipher/-/wikis/home")

  # Default action for arguments is "store", defined explicitly for readability
  parser.add_argument("-i", "--input", dest="file", action="store", 
      help="file containing text to process \nif this flag is not set, it will read from standard input through redirection or keyboard input")
  parser.add_argument("-v", "--verbose", action="count", default=0, 
      help="enable more detailed output")
  
  parser.add_argument("-G", "--graph", action="store_true",
      help="show graph of letter frequencies")
  # Using 'const=' instead of default as this works with 'nargs="?"',
  # meaning that the const is set only if the flag is provided AND no argument is provided
  parser.add_argument("-L", "--list", action="store", type=int, const=5, nargs="?",
      help="show list of top N ")
  parser.add_argument("-C", "--caesar", action="store_true",
      help="analyse input as possible Caesar cipher and look for likely offset distances")

  # Args need to be accessible outside this function
  global args
  args = parser.parse_args()

  if not args.graph and not args.list and not args.caesar:
    print("No output requested. Add '-G', '-L', and/or '-C' to specify what reports to print.")

# Initialize starting string from input file or stdin
def init_str():
  # Input string and resulting string
  global istr

  # If there is a filename, read from it
  if args.file:
    if args.verbose: print(f"Reading from {color.bl}{args.file}{color.nm}")
    istr = open(args.file, "r").read()
  # If no filename, reads from stdin with same behavior as 'cat' in bash edition
  else:
    # String needs to be initialized before it can be appended to
    istr = ''
    if args.verbose:
      if platform.system() == 'Windows':
        print(f"No filename, reading from stdin ('{color.bl}Ctrl-Z, Enter{color.nm}' to detach if interactive)")
      else:
        print(f"No filename, reading from stdin ('{color.bl}Ctrl-D{color.nm}' to detach if interactive)")
    for line in iter(sys.stdin.readline, ''):
      istr += line
  
  # Strip leading and trailing newlines from input string (bash does this automatically)
  istr = istr.rstrip()

# Take a dict of letter counts and turn it into frequency
def countfreq(count: dict, total: int):
  freq = {}

  for k, v in count.items():
    freq[k] = round(((v / total) * 100), 1)
  
  return freq

# Print frequency distribution
def distprint(freq: dict, maxval: float):
  try:
    columns, lines = os.get_terminal_size()
  except:
    # If unable to get size, default to 80 columns
    columns = 80
  # Set bars to 10 less than the total width of the terminal
  bar_width = columns - 10
  # Scale according to maximum value
  scale = bar_width / maxval

  for k, v in freq.items():
    bar_len = round(v * scale)
    # Use string multiplication to construct a string with
    # the correct amount of fill character and space
    bar = ("█" * bar_len).ljust(bar_width)
    print("{} {:>5} |{}|".format(k, v, bar))

# Specially format tuples for topitems
def tupform(tup):
  # Recognize tuples with multiple items
  if isinstance(tup, tuple) and len(tup) > 1:
    return f"{tup[0]}: {tup[1]}"
  # Regularly print other types
  else:
    return str(tup)

# Print top NUM items of dict or list
def topitems(num: int, iter1, iter2=''):
  # Initialize the variables that will be printed
  sorted1 = iter1
  sorted2 = iter2

  # Handle dicts in both possible positions
  # Turns a dict into a list of tuples
  if isinstance(iter1, dict):
    # Sort by the value of the dict in reverse order (larger to smaller)
    sorted1 = sorted(iter1.items(), key=lambda item: item[1], reverse=True)
  if isinstance(iter2, dict):
    sorted2 = sorted(iter2.items(), key=lambda item: item[1], reverse=True)
  
  # Loop through values for both sorted lists
  # Using 'zip_longest()' to iterate through both until the *longer* list is exhausted
  # Setting the 'fillvalue' so that once one is exhausted it's just empty
  for v1, v2 in zip_longest(sorted1[:num], sorted2[:num], fillvalue=''):
    # Use 'tupform()' to format any tuples that might appear in the loop
    # and using 'ljust' to make the second set appear in a consistent position
    # and to have it line up with the titles in the lists function
    print(f"{tupform(v1).ljust(18)}{tupform(v2)}")

# Compare floats based on some error margin
def closeenough(n1: float, n2: float, margin: float=margins.coarse):
  if args.verbose > 2: print(f"Comparing {n1} and {n2} with margin {margin}")

  # The math.isclose() function allows specifying relative or absolute tolerances or both
  # However, we've got them split up here so that we can tell which tolerance was matched
  rel = abs = False
  # Test with relative tolerance (relative to the larger number)
  if isclose(n1, n2, rel_tol=margin):
    rel = True
    if args.verbose > 2: print("  Relatively close")
  # Test with absolute tolerance
  if isclose(n1, n2, abs_tol=margin):
    abs = True
    if args.verbose > 2: print("  Absolutely close")

  # Return either boolean if one is true
  return rel or abs

##### Core functions

# Count letters in input string
def count_letters():
  # Allow global access to counts and letter frequency
  global count, freq, paircounts, doublecounts, letters
  if args.verbose > 0: print("Analysing letter frequency")
  # lowercase working string for simplicity
  wstr = istr.lower()

  # Variable for count of letters in string
  letters = 0
  # Initalize dict of alphabetic letters with their count in the input
  count = dict.fromkeys(alpha, 0)

  # Initialize dict for letter pair counts
  # Using defaultdict so uninitialized keys are assumed to have 0 value
  paircounts = defaultdict(int)
  # Another for doubles of the same letter
  doublecounts = defaultdict(int)

  # Loop through individual characters in wstr.
  # Using for loop so we can look at both current and next characters
  for i in range(len(wstr)-1):
    # Get current and next letters in wstr
    try:
      cur = wstr[i]
    except:
      # If can't get cur, just increment i and go to next loop run
      i += 1
      continue
    try:
      nxt = wstr[i + 1]
    except:
      # If can't get nxt, probably end of string
      # For end of string, nxt should be blank (to be padded with 'x') and continue with that
      nxt = ''

    if cur.isalpha():
      # If it's an alphabetic char, increment the letter count
      # and the count for the current letter
      letters += 1
      count[cur] += 1
      # Also check if nxt is alphabetic
      if nxt.isalpha():
        pair = cur + nxt
        # Increment the appropriate pair of letters
        # Due to defaultdict, don't need to check if the key exists or not
        if cur == nxt:
          # If equal, it's a double
          doublecounts[pair] += 1
        else:
          # Otherwise it's a regular pair
          paircounts[pair] += 1
  
  # Make new dict with frequencies that can be compared to English frequencies
  freq = countfreq(count, letters)

# Show graph of letter frequencies
def show_graph():
  max_eng = max(engfreq.values())
  max_str = max(freq.values())
  max_all = max(max_eng, max_str)

  if args.verbose > 1:
    print(f"Letter count: {letters}")
    print(f"Max frequency: {max_all}")

  print(f"\n == Typical English distribution:")
  distprint(engfreq, max_all)
  
  print(f"\n == Input string distribution (of {letters} letters):")
  distprint(freq, max_all)

# Show lists of common English and Input letters, pairs, and doubles
def show_lists():
  print(f"\n == Top {args.list} single letters:")
  print(f"Typical English / Input string (of {letters} letters)")
  topitems(args.list, engfreq, freq)

  print(f"\n == Top {args.list} letter pairs:")
  print("Typical English / Input string")
  topitems(args.list, engpairs, paircounts)

  print(f"\n == Top {args.list} letter doubles:")
  print("Typical English / Input string")
  topitems(args.list, engdoubles, doublecounts)

# Look for signs of a Caesar cipher
def like_caesar():
  # Warn of imprecision if less than 100 letters
  if letters < 100:
    print("  Input string too short to provide accurate Caesar inferences.")
  # Lists for low-, med-, and high-confidence suspected offsets
  offset_low = []
  offset_med = []
  offset_high = []

  # Loop through all alphabetic letters with indices
  # This low confidence loop looks for frequencies that might be like 'x' and 'z';
  # basically, two letters with low frequencies separated by one other letter ('y')
  for i, l in enumerate(alpha):
    if args.verbose > 2: print(f"Looking at {l} as x . . .")
    # Check if the input string frequency of l is close to 'x'
    if closeenough(engfreq['x'], freq[l], margins.coarse):
      # If it is check if l+2 is close to 'z'
      if closeenough(engfreq['z'], freq[alpha[(i+2) % len(alpha)]], margins.coarse):
        o = (i+3) % 26
        if args.verbose > 2: print(f"  Looks similar (offset {o}).")
        offset_low.append(o)
  
  # If no low confidence offsets, it doesn't look like a Caesar cipher
  if not offset_low: print("Doesn't look like a Caesar cipher.")

  # Sort low confidence offsets
  offset_low.sort()

  # Loop through low confidence offsets to see which can be upgraded
  for o in offset_low:
    if args.verbose > 1: print(f"Closer look at offset {o} . . .")
    # This counter tracks each time the letter frequencies aren't close enough.
    misses = 0
    for i, l in enumerate(alpha):
      # il is the input letter correlating to l based on offset o
      il = alpha[(i+o) % len(alpha)]
      if not closeenough(engfreq[l], freq[il], margins.precise):
        # If it isn't close enough, log a miss
        if args.verbose > 1: print(f"  Miss: typical English {l} and input {il} are not close enough.")
        misses += 1
    
    # If no misses, the offset is given High confidence
    if misses == 0:
      if args.verbose > 1: print("  Yep, looks likely; upgrading to High confidence.")
      offset_low.remove(o)
      offset_med.append(o)
    # If less than 5 misses, the offset is given Medium confidence
    elif misses < 5:
      if args.verbose > 1: print("  Hmm, looks plausible; upgrading to Medium confidence.")
      offset_low.remove(o)
      offset_med.append(o)
    # Otherwise, left at Low confidence
    else:
      if args.verbose > 1: print("  Nope, too many misses; still Low confidence")

  # If there are Low confidence offsets, report them
  if offset_low:
    # Setting end='' so they all appear on one line
    print("Low confidence Caesar offsets:", end='')
    for o in offset_low:
      print(f" {o}", end='')
    # Ending newline to compensate for end=''
    print()

  # Same thing for Medium confidence
  if offset_med:
    print("Medium confidence Caesar offsets:", end='')
    for o in offset_med:
      print(f" {o}", end='')
    print()

  # Same thing for High confidence
  if offset_high:
    print("High confidence Caesar offsets:", end='')
    for o in offset_high:
      print(f" {o}", end='')
    print()

get_args()
init_str()
count_letters()

if args.graph: show_graph()

if args.list: show_lists()

if letters < 100:
  print("  Input is shorter than 100 letters; results are likely to be imprecise.")

if args.caesar: like_caesar()
