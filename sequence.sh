#!/bin/bash

# A wrapper script for running ciphers in sequence

# Working string to pass between 'cipher.sh' runs and print out at the end
STR=""

# Loop through provided arguments
for arg do
  if [ $arg == $1 ]; then 
    # On the first argument, do not use here string; read from input to 'sequence.sh'
    STR=$(./cipher.sh -y$arg)
  else
    # On other arguments, use $STR as input
    STR=$(./cipher.sh -y$arg <<< $STR)
  fi
done

# Print the resulting string
echo "$STR"
