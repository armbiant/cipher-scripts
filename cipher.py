#!/usr/bin/python3


########## Imports


import argparse, sys, os, re, string, platform, random

from urllib.request import Request, urlopen
from urllib.error import URLError
from datetime import datetime
from enum import Enum, auto
from functools import total_ordering
from typing import Iterable
from textwrap import dedent


########## Classes


##### Supporting Classes


# Smart enum class with canonicalization and dual representations
class SmartEnum(Enum):
  # Initialize new enums with value *and* string representation
  def __new__(cls, value, string_repr) -> Enum:
    obj = object.__new__(cls)
    obj._value_ = value
    obj.string_repr = string_repr
    return obj

  # Catch string conversions of enum
  def __str__(self) -> str:
    return self.string_repr

  # Canonicalize from value or string input
  @classmethod
  def canonicalize(cls, input) -> Enum:
    input_type = type(input)
    # If it is already of the correct type, return it back
    if input_type == cls:
      return input
    elif input_type == int:
      for member in cls:
        if member.value == input:
            return member
    elif input_type == str:
      for member in cls:
        if str(member) == input:
            return member
    else:
      raise TypeError(f"Cannot canonicalize {cls} from {input_type}")
    # If we get to this point then we tried and failed to find a matching value
    raise ValueError(f"No member in {cls} matching {input}")
  

# Class for expanded printing capabilities
class Logger:
  # All valid levels and names
  levels = {
    -1: "quiet",
    0: "normal",
    1: "info",
    2: "tutorial",
    3: "stepwise"
  }
  # Enum form of levels, with both string and int representations
  @total_ordering
  class Level(SmartEnum):
    QUIET = -1, "quiet"
    NORMAL = 0, "normal"
    INFO = 1, "info"
    TUTORIAL = 2, "tutorial"
    STEPWISE = 3, "stepwise"

    # Canonicalize from value or string input
    @classmethod
    def canonicalize(cls, input) -> Enum:
      try:
        return super().canonicalize(input)
      except ValueError:
        highest_member = max(cls, key=lambda member: member.value)
        return highest_member
    
    # Allow comparisons between these enums (only this class needs comparisons)
    def __lt__(self, other) -> bool:
      if self.__class__ is other.__class__:
        return self.value < other.value
      return NotImplemented
    
  # Globabl verbosity level defaults
  print_level = Level.NORMAL
  file_level = Level.TUTORIAL
  # File name to write to
  # Find directory of script
  script_loc = os.path.dirname(os.path.realpath(__file__))
  # Create a directory in that path in an os-agnostic way
  file_name = os.path.join(script_loc, "cipher.log")
  # Open that file path for writing (overwrites existing content)
  file_obj = open(file_name, 'w')
  # Regex for finding color codes
  ansi_escape = re.compile(r'(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]')

  @classmethod
  def set_print_level(cls, new_level) -> None:
    cls.print_level = cls.Level.canonicalize(new_level)

  # Method to print based on globabl verbosity
  @classmethod
  def output(cls, message_level=Level.NORMAL, message='') -> None:
    # Greater than or equal allows the checks to be a little more intuitive;
    # the message level passed to the function should represent
    # the verbosity level at which the provided args should be passed to print().
    if cls.print_level >= message_level:
      print(message)
    # All things printed to console will be added to the file,
    # plus all things included in the file_level
    if cls.print_level >= message_level or cls.file_level >= message_level:
      cls.output_file(str(message_level), message)
  
  # Method to print to file object
  @classmethod
  def output_file(cls, level_desc: str, message=''):
    # Remove escape sequences from message
    plain_message = cls.ansi_escape.sub('', str(message))
    # Construct prefix by getting date and level description
    prefix = f"{datetime.now()}{level_desc.rjust(10)}: "
    # Print line by line so multiline strings each have prefixes
    # (this also seems to cause it to ignore empty messages)
    for line in plain_message.splitlines():
      print(prefix + line, file=cls.file_obj)

  # Error printing method
  @classmethod
  def error(cls, message) -> None:
    print(message, file=sys.stderr)
    cls.output_file("ERROR", message)
  
  # Wrappers for output() to simplify level specifications
  @classmethod
  def normal(cls, message='') -> None:
    return cls.output(cls.Level.NORMAL, message)
  
  @classmethod
  def info(cls, message='') -> None:
    return cls.output(cls.Level.INFO, message)
  
  @classmethod
  def tutorial(cls, message='') -> None:
    return cls.output(cls.Level.TUTORIAL, message)
  
  @classmethod
  def stepwise(cls, message='') -> None:
    return cls.output(cls.Level.STEPWISE, message)


# Simple class for color codes
class color:
  bl = '\033[1m'
  nm = '\033[0m'


# Simple class for color codes
class Color:
  # General color codes
  bold = '\033[1m'
  clear = '\033[0m'

  # Highlight (bold) the provided content
  @classmethod
  def highlight(cls, content: str) -> str:
    # Return highlighted string
    return cls.bold + str(content) + cls.clear


# Class for calculating runtime of code chunks
class RunTime:
  def __init__(self, desc="unspecified task", full_output=True) -> None:
    self.starttime = datetime.now()
    self.desc = desc
    self.full_output = full_output
    # Initialize these attributes to None for now
    # so that we don't get AttributeError if they're referenced before they're set.
    self.endtime = self.elapsed = self.lastend = None
  
  # String method works even if called before end() is called
  def __str__(self) -> str:
    if self.elapsed: 
      if self.full_output: 
        if self.lastend: return f"    - {self.desc} took {self.elapsed} ({self.endtime - self.lastend} since last end)"
        else: return f"    - {self.desc} took {self.elapsed}"
      else: return f"{self.elapsed.seconds}.{str(self.elapsed.microseconds).rjust(6, '0')}"
    else: return f"    - {self.desc} started at {self.starttime} and hasn't yet finished"
  
  def end(self) -> None:
    # If there is already an end, keep it around as a comparison point
    if self.endtime: self.lastend = self.endtime
    # Set end time and calculate delta
    self.endtime = datetime.now()
    self.elapsed = self.endtime - self.starttime
    if args.runtime:
      print(self)


##### Core classes


# Generic cipher class for inheritance
class Cipher:
  # Enumerate standardized case designators
  class Case(Enum):
    NONE = auto()
    ORIGINAL = auto()
    LOWER = auto()
    UPPER = auto()

    # Canonicalize case designator, with substantially different logic from SmartEnum
    @classmethod
    def canonicalize(cls, case_str: str) -> Enum:
      # Recognize partial matches to cases
      # If a full case designator starts with the provided designator...
      if "capital".startswith(case_str.lower()) or "uppercase".startswith(case_str.lower()):
        # assign the appropriate abbreviation
        return cls.UPPER
      elif "small".startswith(case_str.lower()) or "lowercase".startswith(case_str.lower()):
        return cls.LOWER
      elif "none".startswith(case_str.lower()) or "original".startswith(case_str.lower()):
        return cls.ORIGINAL
      else:
        message = f"Invalid case designator '{case_str}'"
        message += "\nSpecify all or part of 'capital', 'uppercase', 'small', or 'lowercase'"
        raise ValueError(message)
  
  # Enumerate cipher operations
  class Operation(SmartEnum):
    ENCIPHER = auto(), "en"
    DECIPHER = auto(), "de"
  
  # Enumerate input types
  class InputType(Enum):
    UNKNOWN = "unknown"
    DIGITS = "digits"
    LETTERS = "letters"

  # Enumerate assumptions
  class Assumption(SmartEnum):
    NONE = auto(), ""
    NO = auto(), "N"
    YES = auto(), "Y"

  # URL for text generation
  GENURL = "https://randomword.com/paragraph"
  # Default controls for all ciphers
  assume = Assumption.NONE
  output_case = Case.NONE
  should_remove_spaces = False

  def __init__(self, input: str, operation=Operation.ENCIPHER) -> None:
    self.input = input
    self.result = ""
    self.operation = self.Operation.canonicalize(operation)
    self.expects = self.InputType.LETTERS
    self.intro = "generic cipher"
    # Whether to use stepwise function in tutorial
    self.use_stepwise = False
    # This should be a list of dictionaries containing messages and actions
    self.tutorial = []
  
  # Initialize standard alphabet list
  def alphabet_init(self) -> None:
    # Iterable list of lowercase letters
    self.alpha_list = list(string.ascii_lowercase)
    # Save separate upper and lowercase strings
    self.lowercase_letters = string.ascii_lowercase
    self.uppercase_letters = string.ascii_uppercase

  # Noop function for tutorial construction
  @staticmethod
  def noop() -> None:
    pass

  # Standardized list formatting for tutorials
  @staticmethod
  def format_list(input: Iterable, width=2, prefix=" ") -> str:
    result = prefix
    for item in input:
      result += str(item).ljust(width)
    return result
  
  # Standardized printing of labeled attributes
  @staticmethod
  def format_labeled(content: str, label="input", width=10, separator="=") -> str:
    #return f"{label.rjust(width)} {separator} {content}"
    return " ".join([label.rjust(width), separator, content])
  
  # Similar to above but speficially for stepwise tutorials showing per-char change
  @staticmethod
  def format_change(old_content: str, new_content: str, old_width=5, separator="->") -> str:
    # Add quotes to right and new content
    old_content = f"'{old_content}'"
    new_content = f"'{new_content}'"
    old_content = old_content.rjust(old_width)
    return " ".join([old_content, separator, new_content])

  # Set case for parent cipher class
  @classmethod
  def set_case(cls, case_str: str) -> None:
    cls.output_case = cls.Case.canonicalize(case_str)
  
  # Wait for Enter to continue, used in tutorials
  @staticmethod
  def wait_enter(action="Solve the cipher") -> None:
    # Try to wait for Enter to continue, else just print a newline and continue
    try:
      input(f"{action} on your own if you wish, then press Enter to continue: ")
    except:
      print()
    # Extra newline to get the spacing right
    print()

  # Determine what the input string contains
  def get_input_type(self) -> str:
    # Determine content type by first or second letter of input
    if re.search('^.?[0-9]', self.input):
      return self.InputType.DIGITS
    elif re.search('^.?[A-Za-z]', self.input):
      return self.InputType.LETTERS
    else:
      return self.InputType.UNKNOWN
  
  # Old idea for implementing tutorials; might revisit later
  # Add item to tutorial to ensure correct typing
  # def add_tutorial_item(self, message: str, wait_action: tuple, next_action: function) -> None:
  #   if type(wait_action[0]) != function:
  #     raise TypeError("The first item in 'wait_action' must be a function.")

  #   newitem = {
  #     "message": message,
  #     "wait_action": wait_action,
  #     "next_action": next_action
  #   }

  # Generic tutor method
  # def tutor(self) -> None:
  #   for item in self.tutorial:
  #     message = item["message"]
  #     print(message)

  # Print warning if type doesn't match expectation
  def check_type(self) -> bool:
    input_type = self.get_input_type()
    if input_type == self.expects:
      return True
    else:
      # If an assumption has been specified, return from the function with the appropriate return value
      if self.assume == self.Assumption.YES: return True
      elif self.assume == self.Assumption.NO: return False
      else:
        Logger.error(f"String starts with {input_type.value}, which the specified cipher doesn't use - unlikely to be useful.")
        Logger.error("(Add '-y' or '-n' to provide an answer non-interactively.)")
        return True
  
  # Remove spaces from string
  @staticmethod
  def remove_spaces(input: str) -> str:
    # Implemented by splitting the output into iterable list,
    # then joining it with no char as the separator
    return ''.join(input.split())

  # Get string with report string
  def report(self, intro=None, intro_level=Logger.Level.INFO) -> str:
    intro = intro or self.intro
    Logger.output(intro_level, f"  Processed with {intro}:")
    output = self.result

    # If strip spaces requested, perform that change here
    if self.should_remove_spaces: 
      output = self.remove_spaces(output)
    
    # Handle upper/lower case
    if self.output_case == self.Case.UPPER:
      output = output.upper()
    elif self.output_case == self.Case.LOWER:
      output = output.lower()
    
    # Output the processed string
    Logger.output(Logger.Level.NORMAL, output)
  
  # Standard flow for running a cipher operation
  def run(self) -> None:
    # Check for valid type
    if self.check_type():
      # Run tutor if applicable
      if Logger.print_level >= Logger.Level.TUTORIAL:
        self.tutor()
      # Process the cipher
      self.process()
      # Almost all the ciphers should report their status after running.
      # Those that don't can wrap the main_report() method with additional checks.
      self.main_report()
  
  # Generic processs method
  def process(self) -> None:
    pass  # Does nothing here, just makes it so the generic run method can apply to all child classes
  
  # Generic tutor method
  def tutor(self, input=None) -> None:
    # If set, print tutor string
    if hasattr(self, 'tutor_string') and self.tutor_string:
      # Pass string through dedent to remove indentation inherited from 
      # multiline string construct
      Logger.tutorial(dedent(self.tutor_string))
    # The introduction to the tutor will vary by the inidivual cipher,
    # but this logic will apply to several.
    # Offer user the opportunity to solve the cipher on their own,
    # then go stepwise if requested.
    self.wait_enter()
    # Check if we should use the stepwise function
    if self.use_stepwise and Logger.print_level >= Logger.Level.STEPWISE:
      self.stepwise(input)

  # Generic stepwise method
  def stepwise(self, input=None) -> None:
    # Input if provided, else self.input
    input = input or self.input
    # Loop through characters in the provided input
    for c in input:
      if c.isalpha():
        # Process alphabetic characters and report result
        self.process(c)
        Logger.output(Logger.Level.STEPWISE, self.format_change(c, self.result))
      elif c == "\n":
        # Print extra newline to make newlines stand out
        Logger.output(Logger.Level.STEPWISE, "  newline\n")
      else:
        Logger.output(Logger.Level.STEPWISE, self.format_change(c, c))
    # Extra newline for spacing
    Logger.output(Logger.Level.STEPWISE, "")
  
  # Generic wrapper for report()
  def main_report(self) -> None:
    self.report()


# Caesar cipher (default to 3-char offset)
class Caesar(Cipher):
  # Enumerate directions
  class Direction(SmartEnum):
    RIGHT = auto(), 'R'
    LEFT = auto(), 'L'

  def __init__(self, input: str, offset=3, direction=Direction.RIGHT, brute_mode=False) -> None:
    super().__init__(input)
    # Set additional Caesar attributes
    self.offset = offset
    self.direction = self.Direction.canonicalize(direction)
    self.brute_mode = brute_mode
    self.intro = Color.highlight(f"Caesar {self.direction} {self.offset}")
    # This cipher needs the stepwise function for tutorial
    self.use_stepwise = True
    self.alphabet_init()
  
  # Initialize translation table
  def alphabet_init(self) -> None:
    super().alphabet_init()
    # Shift lower and upper case letters by 1
    lowercase_translation = self.lowercase_letters[1:] + self.lowercase_letters[:1]
    uppercase_translation = self.uppercase_letters[1:] + self.uppercase_letters[:1]

    # Create translation table by combining lowercase and uppercase translations
    if self.direction == self.Direction.LEFT:
      # Order depends on the direction
      self.table = str.maketrans(lowercase_translation + uppercase_translation,
                                self.lowercase_letters + self.uppercase_letters)
    else:
      self.table = str.maketrans(self.lowercase_letters + self.uppercase_letters,
                                lowercase_translation + uppercase_translation)
  
  # Initialize tutorial information
  # def tutorinit(self) -> None:
  #   first_message = f"""
  #            == Caesar Cipher tutorial ==
            
  #           The Ceasar Cipher shifts all letters in the string by DIST positions in the alphabet.
  #           The default Caesar distance is 3.
  #           Typically a RIGHT shift is used to encipher text and LEFT to decipher.

  #           To perform the Caesar cipher with the current settings,
  #           1. Locate each character in the input string
  #           2. Replace it with the character that is {self.offset} spaces to the {self.direction.value}
  #               a. Pass through spaces, punctuation, other special characters

  #           {'input'.rjust(10)} = {input}
  #           {'operation'.rjust(10)} = {self.offset} {self.direction.value}

  #           {' '.join(self.alpha_list)}
  #           """
  #   # self.add_tutorial_item(first_message, (self.wait_enter,), (self.noop,))

  # Provide tutorial for Caesar cipher
  def tutor(self, input=None) -> None:
    # Input if provided, else self.input
    input = input or self.input

    self.tutor_string = f"""
     == Caesar Cipher tutorial ==

    The Ceasar Cipher shifts all letters in the string by DIST positions in the alphabet.
    The default Caesar distance is 3.
    Typically a RIGHT shift is used to encipher text and LEFT to decipher.

    To perform the Caesar cipher with the current settings,
     1. Locate each character in the input string
     2. Replace it with the character that is {self.offset} spaces to the {self.direction}
        a. Pass through spaces, punctuation, other special characters

    {self.format_labeled(self.input)}
    {self.format_labeled(f"{self.offset} {self.direction}", label="operation")}

    {self.format_list(self.alpha_list)}
    """
    # Finish with inherited tutor logic
    super().tutor(input)

  # Process input with the configured Caesar cipher
  def process(self, input=None) -> None:
    # We should process the provided input value,
    # or self.input if none provided
    self.result = input or self.input

    # Account for large offsets depending on the set mode
    if self.brute_mode:
      # In brute force mode, we should try every offset until 25
      # (after that it's equivalent to the original string)
      self.offset = min(self.offset, 25)
    else:
      # In regular mode, the modulo will ensure it ends up in the right place
      # (even for negative offsets)
      self.offset = self.offset % 26
    
    for i in range(self.offset):
      self.result = self.result.translate(self.table)
      # In brute force mode, report every offset
      if self.brute_mode:
        # Unlike bash, the loop goes from 0 to (offset-1).
        # Increment i in output to make it reflective of how many have run
        # and use intro level normal so it always prints.
        self.report(f"Offset {i+1}", Logger.Level.NORMAL)
  
  # Override main report to check brute force mode
  def main_report(self) -> None:
    # In brute force mode, reporting is handled within the process function
    if not self.brute_mode:
      super().report()


# Atbash cipher (reversed letters)
class Atbash(Cipher):
  def __init__(self, input: str, operation=Cipher.Operation.ENCIPHER) -> None:
    super().__init__(input, operation)
    self.intro = Color.highlight("Atbash")
    # This cipher needs the stepwise function for tutorial
    self.use_stepwise = True
    self.alphabet_init()

  def alphabet_init(self) -> None:
    super().alphabet_init()
    # Reverse lower and upper case letters (these will be useful outside of this method)
    self.lowercase_translation = self.lowercase_letters[::-1]
    self.uppercase_translation = self.uppercase_letters[::-1]
    # Create translation table
    self.table = str.maketrans(self.lowercase_letters + self.uppercase_letters,
                              self.lowercase_translation + self.uppercase_translation)
  
  def tutor(self, input=None) -> None:
    # Input if provided, else self.input
    input = input or self.input

    self.tutor_string = f"""
     == Atbash Cipher tutorial ==

    The Atbash Cipher translates all letters in the string to their complement in the alphabet.
    It is a reciprocal cipher; the same steps are used to encipher and decipher text.

    To perform the Atbash cipher,
     1. Locate each character in the normal alphabet
     2. Replace it with the character in the same position in the reversed alphabet
        a. Pass through spaces, punctuation, other special characters
    
    {self.format_labeled(self.input)}

    {self.format_list(self.alpha_list)}
    {self.format_list(reversed(self.alpha_list))}
    """
    super().tutor(input)

  def process(self, input=None) -> None:
    # Input if provided, else self.input
    input = input or self.input
    # Perform translation
    self.result = input.translate(self.table)


# A1Z26 cipher (numbered position in alphabet)
class A1Z26(Cipher):
  def __init__(self, input: str, operation=Cipher.Operation.ENCIPHER) -> None:
    super().__init__(input, operation)
    self.intro = Color.highlight("A1Z26")
    self.alphabet_init()
  
  def alphabet_init(self) -> None:
    super().alphabet_init()
    # For this cipher, we want the alphabet list to have an empty first item for two reasons:
    # 1. This puts 'a' at index 1, so we can directly use A1Z26 numbers as the array index
    # 2. This allows appending alpha[0] to the resulting string ad nauseam with no consequences
    self.alpha_list.insert(0, "")

  # For this cipher we don't check type since it works with both
  def check_type(self) -> bool:
    return True   # Type is always correct
  
  # General tutor information
  def tutor(self) -> None:
    self.tutor_string = f"""
     == A1Z26 Cipher tutorial ==

    The A1Z26 Cipher represents letters by their numeric position in the alphabet.
    This is summarized in the name; A=1, Z=26.

    To perform the A1Z26 cipher,
     1. Locate each character or number in the input string
     2. Replace it with the correlating item of the other type
        a. If you are enciphering into numbers, add dashes ('-') between successive indices
        b. If you are deciphering from numbers, remove the dashes between the indices
        c. Pass through spaces, punctuation, other special characters
    
    {self.format_labeled(self.input)}

    {self.format_list(range(1,27), width=3)}
    {self.format_list(self.lowercase_letters, width=3)}
    """
    super().tutor()

  # Main processing functionality
  def process(self, input=None) -> None:
    input = input or self.input
    new_index = 0
    # Iterate over characters in input
    for char in input:
      # Original and new content added this round
      old_char = new_char = ''
      # Python added case-like match structures in 3.10
      # Using if-elif instead for better compatibility
      if char.isalpha():
        old_char += char
        # If the current last character in the resulting string is a digit,
        # we will need a '-' to distinguish the different indices 
        if re.search('[0-9]$', self.result): 
          # Also need to check that the string doesn't end with a newline
          # Not necessary in bash due to different handling of the $ anchor
          if not re.search('\n$', self.result):
            new_char += '-'
        # Append the index of the input char
        new_char += str(alpha.index(char.lower()))
      elif char.isdigit():
        # Add the digit character to the index variable.
        # Because Python actually cares about types, 
        # we're concatenating as strings then casting back to int
        new_index = int( str(new_index) + char )
      else:
        if new_index != 0:
          old_char += str(new_index)
        # A dash (or any other character) indicates the end of a numbered index.
        # Add to the resulting string the alphabet character at the current index.
        # This sometimes adds the first item in the array to the string,
        # which doesn't matter since the first item is ''
        new_char += alpha[new_index]
        # Clear the index variable for next time
        new_index = 0
        # If the special character is anything other than a newline,
        # we should report it as part of the original content
        if char != '\n':
          old_char += char
          # If the special character is also not a dash, should be passed on to result
          if char != '-': new_char += char
      # If new content is not empty, report change
      if new_char != '':
        Logger.output(Logger.Level.STEPWISE, self.format_change(old_char, new_char, old_width=6))
      # Append new content to resulting string
      self.result += new_char
      # Account for newline characters separately for better step-by-step output
      if char == '\n':
        self.result += char
        Logger.output(Logger.Level.STEPWISE, "    newline\n")
    
    # One more append in case the last character is a digit
    self.result += self.alpha_list[new_index]
    # Report extra append if step-by-step
    if new_index != 0:
      new_char += self.alpha_list[new_index]
      Logger.output(Logger.Level.STEPWISE, self.format_change(old_char, new_char))
      # Extra newline
      Logger.output(Logger.Level.STEPWISE)


# Vigenere cipher (offset cipher based on per-letter alphabetic index in provided key)
class Vigenere(Caesar):
  def __init__(self, input: str, key: str, operation=Cipher.Operation.ENCIPHER, direction=Caesar.Direction.RIGHT, use_autokey=False) -> None:
    # The Vigenere cipher is kind of a variant of the Caesar cipher
    # Perform normal cipher initialization
    Cipher.__init__(self, input, operation)
    # And caesar initialization
    Caesar.__init__(self, input, direction=direction)
    # Additional Vigenere-specific stuff
    # Set key without any spaces
    self.key = self.remove_spaces(key)
    self.use_autokey = use_autokey
    self.intro = Color.highlight(f"Vigenere {direction} {key}")
    self.alphabet_init()
  
  def tutor(self) -> None:
    self.tutor_string = """
     == Vigenere Cipher tutorial ==

    The Vigenere Cipher is a more elaborate Caesar cipher in which the offset varies per letter.
    The key provided to the cipher determines this per-letter offset.
    """
    # Need extra tutor information if using autokey variant
    if self.use_autokey:
      self.tutor_string += """
      The Autokey variant has been selected, so the plain text should be appended to the key.
      """
      if self.operation == Cipher.Operation.ENCIPHER:
        self.tutor_string += f"It is currently set to {Color.highlight(self.operation)}, so the input text can be immediately appended to the key."
      else:
        self.tutor_string += f"It is currently set to {Color.highlight(self.operation)}, so the plain text must be appended to the key as it is deciphered."
    self.tutor_string += f"""
    To perform the Vigenere cipher,
     1. Line up each alphabetic input character with the corresponding alpha or numeric character in the key
        a. Pass through all non-alpha input characters
        b. Repeat the key as needed
     2. Locate the key character's index in the alphabet (or literal value if a digit)
     3. Replace the input character with the character that many characters to the {self.direction}
    """
    # Additional autokey special sauce
    if self.use_autokey and self.operation == Cipher.Operation.DECIPHER:
      self.tutor_string += "    a. (Autokey) Add the new character to the end of the key"
    self.tutor_string += f"""
    {self.format_labeled(self.input)}
    {self.format_labeled(self.key, label="key")}
    {self.format_labeled(f"{self.operation} {self.direction}", label="operation")}

    {self.format_list(range(0,26), width=3)}
    {self.format_list(self.lowercase_letters, width=3)}
    """
    super().tutor()

  def process(self) -> None:
    pass


########## Functions


##### Supporting functions


# Error printing method to match bash script functionality
def errprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

# Remove spaces from string
def nospace(wstr):
  # Implemented by splitting the output into iterable list,
  # then joining it with no char as the separator
  return ''.join(wstr.split())

# Function to get args for more easy navigation of the code
# Using argparse, so help (usage()) and variable init (init_vars()) is handled here as well
def get_args():
  # Initialize parser 
  # No built-in help so we can add it separately to the "Script modifiers" group
  # Using alternate formatter so the longer description can be used
  # Setting description and epilog for usage synopsis
  parser = argparse.ArgumentParser(add_help=False, formatter_class=argparse.RawTextHelpFormatter, 
  description='''
  Cipher script
  Written by Stephen Kendall

  Capable of deciphering and enciphering with 
    Caesar, Atbash, A1Z26, Vigenere, Beaufort, Keyword, Playfair, and Bifid.
  The Vigenere implementation supports the Beaufort, Gronsfeld, and Autokey variations.
  If multiple ciphers are requested, the same input string will be processed by each one separately.''', 
  epilog="Consult the Wiki for more details: https://gitlab.com/SirDoctorK/cipher/-/wikis/home")

  ### Script modifiers
  # Default action for arguments is "store", defined explicitly for readability
  smod = parser.add_argument_group("Script Modifiers")
  smod.add_argument("-h", "--help", action="help", 
      help="show this help message and exit")
  smod.add_argument("-i", "--input", dest="file", action="store", 
      help="file containing text to process \nif this flag is not set, it will read from standard input through redirection or keyboard input")
  smod.add_argument("-g", "--generate", dest="gen", action="store_true",
      help=f"Generate input text from '{Cipher.GENURL}'")
  smod.add_argument("-v", "--verbose", action="count", default=0, 
      help="enable more detailed output (-v=info, -vv=tutorial, -vvv=step-by-step)")
  smod.add_argument("--runtime", action="store_true", 
      help="print detailed execution times (for debug)")
  smod.add_argument("-c", "--case", action="store", 
      help="convert output string to the specified case")
  smod.add_argument("-s", "--strip", "--strip-spaces", action="store_true", 
      help="strip space characters from output string")
  
  # Exclusive group for yes/no assumption
  assume = smod.add_mutually_exclusive_group()
  assume.add_argument("-y", "--yes", "--assume-yes", dest="assume", action="store_const", const="Y", 
      help="silently continue if the input string doesn't seem to match the cipher")
  assume.add_argument("-n", "--no", "--assume-no", dest="assume", action="store_const", const="N", 
      help="silently skip cipher; if neither specified, will ask interactively")

  ### Cipher selectors
  ciphers = parser.add_argument_group("Cipher Selectors (at least one required)")
  ciphers.add_argument("-C", "--caesar", action="store_true", 
      help="use the Caesar cipher, default to shift right 3 characters")
  ciphers.add_argument("-O", "--offset", dest="dist", type=int, action="store", 
      help="use the Caesar cipher with the specified distance, inplies '-C'")
  ciphers.add_argument("-A", "--atbash", action="store_true", 
      help="use the Atbash cipher")
  ciphers.add_argument("-Z", "--a1z26", action="store_true", 
      help="use the A1Z26 cipher")
  ciphers.add_argument("-V", "-G", "--vigenere", "--gronsfeld", dest="vkey", action="store", 
      help="use the Vigenere cipher or a variant")
  ciphers.add_argument("-U", "--autokey", dest="ukey", action="store", 
      help="use the Vigenere Autokey cipher")
  ciphers.add_argument("-B", "--beaufort", dest="bkey", action="store", 
      help="use the Beaufort cipher")
  ciphers.add_argument("-K", "--keyword", dest="kkey", action="store", 
      help="use the Keyword cipher")
  ciphers.add_argument("-P", "--playfair", dest="pkey", action="store", 
      help="use the Playfair cipher")
  ciphers.add_argument("-I", "--bifid", dest="ikey", action="store", 
      help="use the Bifid cipher")
  ciphers.add_argument("-S", "--substitute", "--substitution", dest="sub", nargs="+",
      help="provide specific monoalphabetic substitutions to perform")
  ciphers.add_argument("-R", "--random", dest="rand", action="store_true",
      help="randomly generate and apply a substitution alphabet")

  ### Cipher modifiers
  # These flags modify cipher operation
  cmod = parser.add_argument_group("Cipher Modifiers")

  cmod.add_argument("-b", "--brute", "--brute-force", action="store_true", 
      help="modify Caesar to print every shift up to DIST (default 3)")

  # Adding exclusive groups for -r/l and -e/d
  direction = cmod.add_mutually_exclusive_group()
  direction.add_argument("-r", "--right", dest="dir", action="store_const", const="R", 
      help="use a right shift for Caesar and Vigenere ciphers (default)")
  direction.add_argument("-l", "--left", dest="dir", action="store_const", const="L", 
      help="use a left shift for Caesar and Vigenere ciphers")

  operation = cmod.add_mutually_exclusive_group()
  operation.add_argument("-e", "--encipher", dest="op", action="store_const", const="en", default="en", 
      help="encipher input text (default)")
  operation.add_argument("-d", "--decipher", dest="op", action="store_const", const="de", 
      help="decipher input text")

  cmod.add_argument("-p", "--period", action="store" , type=int, default=5, 
      help="specify the period for Bifid to intersperse y- and x-values (deafult 5)")
  ### All arguments set up

  # Args need to be accessible outside this function
  global args
  args = parser.parse_args()

  if args.assume:
    Cipher.assume = Cipher.Assumption.canonicalize(args.assume)
  
  Logger.set_print_level(args.verbose)
  Cipher.should_remove_spaces = args.strip

  # Set default direction
  # Could be done with argparse, but we also need en/de to influence direction
  # if direction is not explicitly specified
  if args.dir is None:
    if args.op == 'de':
      args.dir = "L"
    else:
      args.dir = "R"
  
  # Similar thing with distance, need to also set Caesar value
  if args.dist is None:
    # Default shift direction is 3
    args.dist = 3
  else:
    # If dist is set then -O was probably provided and 
    # Caesar function should run
    args.caesar = True

  # Copy autokey key to main Vigenere variable
  if args.ukey: 
    args.vkey=args.ukey

  # Process case designator into canonical form
  if args.case:
    Cipher.set_case(args.case)
    try: args.case = canoncase(args.case)
    except ValueError: sys.exit(1)
  
  if not args.caesar and not args.atbash and not args.a1z26 and not args.sub and not args.rand \
      and not args.vkey and not args.bkey and not args.kkey and not args.pkey and not args.ikey:
    errprint("No operation specified")
    errprint(f"Use at least one of the Cipher selector flags (see '{color.bl}cipher.py -h{color.nm}' for details)")
    sys.exit(1)

# Initialize starting string from input file, stdin, or random paragraph webpage
def init_str():
  # Input string and resulting string
  global istr, rstr

  # If set to generate input text, fetch that here
  if args.gen:
    if args.verbose: print(f"Fetching generated input text from '{Cipher.GENURL}'")

    # Set user agent in header to avoid 403 forbidden
    hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11'}
    req = Request(Cipher.GENURL, headers=hdr)

    # Fetch contents of the random paragraph page
    # with error handling suggested by Python docs
    try:
      response = urlopen(req)
    except URLError as e:
      istr = ''
      if hasattr(e, 'reason'):
        errprint('We failed to reach the server.')
        errprint('Reason: ', e.reason)
      elif hasattr(e, 'code'):
        errprint('The server couldn\'t fulfill the request.')
        errprint('Error code: ', e.code)
    else:
      html = response.read()
      # Decode response content bytes into string
      randstr = html.decode('utf-8')
      # Grab the part of the string after the opening div bracket, 
      # then grab the part of the string before the closing div bracket
      istr = randstr.split('<div id="random_word_definition">', 1)[1].split("</div>", 1)[0]
  
  # If there is a filename, read from it
  elif args.file:
    if args.verbose: print(f"Reading from {color.bl}{args.file}{color.nm}")
    istr = open(args.file, "r").read()
  
  # If no filename, reads from stdin with same behavior as 'cat' in bash edition
  else:
    # String needs to be initialized before it can be appended to
    istr = ''
    if args.verbose:
      if platform.system() == 'Windows':
        print(f"No filename, reading from stdin ('{color.bl}Ctrl-Z, Enter{color.nm}' to detach if interactive)")
      else:
        print(f"No filename, reading from stdin ('{color.bl}Ctrl-D{color.nm}' to detach if interactive)")
    for line in iter(sys.stdin.readline, ''):
      istr += line
  
  # Strip leading and trailing newlines from input string (bash does this automatically)
  istr = istr.strip()
  # Initialize rstr to istr to avoid errors
  rstr = istr

  # Exit script if the input string is empty
  if istr == '':
    errprint("Empty input string; nothing to do.")
    sys.exit(1)

  # Any verbosity causes the original input string to be printed
  if args.verbose:
    print(f"  Input string:\n{istr}")

# Identify whether string contains primarily digits or alpha
def contains():
  if re.search('^.?[0-9]', istr):
    return "digits"
  elif re.search('^.?[A-Za-z]', istr):
    return "letters"
  else:
    return "unknown"

# Canonicalize case designator
def canoncase(casestr: str):
  # Recognize partial matches to cases
  # If a full case designator starts with the provided designator...
  if "capital".startswith(casestr.lower()) or "uppercase".startswith(casestr.lower()):
    # assign the appropriate abbreviation
    return "C"
  elif "small".startswith(casestr.lower()) or "lowercase".startswith(casestr.lower()):
    return "S"
  elif "none".startswith(casestr.lower()) or "original".startswith(casestr.lower()):
    return "N"
  else:
    errprint(f"Invalid case designator '{casestr}'")
    errprint("Specify all or part of 'capital', 'uppercase', 'small', or 'lowercase'")
    raise ValueError()

# Print warning if string doesn't seem to work with desired cipher
def confirm():
  # If an assumption has been specified, return from the function with the appropriate return value
  if args.assume == "Y": return True
  elif args.assume == "N": return False
  else:
    errprint(f"String starts with {cont}, which the specified cipher doesn't use - unlikely to be useful.")
    errprint("(Add '-y' or '-n' to provide an answer non-interactively.)")
    return True

# Initialize alphabets with specific options for Keyword, Playfair, and Bifid
def alphinit(ciph=None):
  # These variables need to be accessible globally
  global az, azAZ, za, zaZA, czCZ, alpha, no_j

  # Iterable list of alphabetic characters with empty first index for two reasons:
  # 1. This puts 'a' at index 1, so we can directly use A1Z26 numbers as the array index
  # 2. This allows appending alpha[0] to the resulting string ad nauseam with no consequences
  alpha = [ "" ]
  alpha += list(string.ascii_lowercase)  
  # Array index of the start of the untouched alphabet
  # (Alphabet Start Index)
  # All functions that use this pop off the 0th element
  asi=0

  # Key for alphinit to use (default nothing)
  tempkey = ''

  # Prepare translation table to change 'j' to 'i'
  no_j = str.maketrans('j', 'i')

  if ciph == 'b':
    # For Beaufort, we don't want an empty 0th element
    alpha.pop(0)
  elif ciph == 'k':
    tempkey = args.kkey
    alpha.pop(0)
  elif ciph == 'p':
    # Playfair (and Bifid) consider 'j' as 'i', so we will change it in the key
    tempkey = args.pkey.translate(no_j)
    # Need to remove the 0th (blank) element and 'j' from the list
    alpha.pop(0)
    alpha.remove('j')
    # Without empty starting element, need Alphabet Start Index to be 0
    asi = 0
  elif ciph == 'i':
    # Same unique init procedure as Playfair
    tempkey = args.ikey.translate(no_j)
    alpha.pop(0)
    alpha.remove('j')
    asi = 0
  else:
    # Intermediate strings of alphabetic letters
    # In Bash edition, this is done in init_vars, but since Python handles much of that with argparse,
    # I've put these here in alphinit in the default branch
    az = ''.join(alpha)
    AZ = az.upper()
    za = az[::-1]
    ZA = AZ[::-1]

    # Strings actually to be used by Atbash function
    azAZ = az + AZ
    zaZA = za + ZA

    # Construct right-shift alphabet for Caesar
    calpha = list(string.ascii_lowercase)
    calpha.pop(0)
    calpha.append('a')
    
    # Intermediate strings of alphabetic letters
    cz = ''.join(calpha)
    CZ = cz.upper()

    # Right-shift alphabet string to used by Caesar function
    czCZ = cz + CZ
  
  # Construct the substitution alphabet
  # Loop through characters in the key (by default the key is empty)
  for k in tempkey:
    # Verify that k is a letter (if not, nothing to do)
    if k.isalpha():
      # Get the index of the key character
      kin = alpha.index(k.lower())
      # If the key character is at or past the alphabetic start index,
      # we are at its first occurrence in the key
      if kin >= asi:
        # Loop from kin down to asi (if equal, will do nothing)
        for i in range(kin, asi, -1):
          # Move the characters right one space in this range
          alpha[i] = alpha[i - 1]
        # Set the alphabet at asi to key character
        alpha[asi] = k
        # Increment asi
        asi += 1

# Print the current alphabet in 2d for Playfair / Bifid (for debugging/tutorial)
def print2d():
  print("  0 1 2 3 4")
  print(f"0 {' '.join(alpha[0:5])}")
  print(f"1 {' '.join(alpha[5:10])}")
  print(f"2 {' '.join(alpha[10:15])}")
  print(f"3 {' '.join(alpha[15:20])}")
  print(f"4 {' '.join(alpha[20:25])}")

# Get element from array with 2d indices
def get2d(ix: int, iy: int):
  # Perform modulo to account for positive rollover
  ix = ix % 5
  iy = iy % 5

  # Account for negative X-value rollover
  if ix < 0:
    # The X or Y value should be 5 + plus the negative number
    # e.g., 5 + -1  = 5 - 1 = 4 (last column)
    ix = 5 + ix
  
  # Same for negative y-value rollover
  if iy < 0:
    iy = 5 + iy
  
  # Observe the layout of the print2d() function if this doesn't make sense
  idx = iy * 5 + ix

  return alpha[int(idx)]

# Wait for Enter to continue, used in tutorials
def wait_enter(action="Solve the cipher"):
  # Try to wait for Enter to continue, else just print a newline and continue
  try:
    input(f"{action} on your own if you wish, then press Enter to continue: ")
  except:
    print()
  # Extra newline to get the spacing right
  print()


##### Core functions


# Caesar cipher (default to 3-char offset)
def caesar(instr=None):
  # Need to modify rstr
  global rstr
  #caesar_time = runtime(f"processing Caesar with offset ({args.dist})")
  # Set wstr based on provided arg to function
  if instr: wstr = instr
  else: wstr = istr

  # Make Caesar translation table based on desired direction
  if args.dir == 'L':
    ctable = str.maketrans(czCZ, azAZ)
  else:
    ctable = str.maketrans(azAZ, czCZ)

  for i in range(args.dist):
    wstr = wstr.translate(ctable)
    if args.brute:
      # Unlike bash edition, we're using wstr in the function
      # Need to update rstr for the report to work
      rstr = wstr
      # Also unlike bash, it goes from 0 to (dist-1)
      # Increment i in output to make it reflective of how many have run
      print(f"Offset {i+1}: ")
      report()

  rstr = wstr
  #caesar_time.end()

# Caesar tutorial
def caesar_tutor(instr=None):
  print()
  print(" == Caesar Cipher tutorial ==")
  print()
  print("The Ceasar Cipher shifts all letters in the string by DIST positions in the alphabet.")
  print("The default Caesar distance is 3.")
  print("Typically a RIGHT shift is used to encipher text and LEFT to decipher.")
  print()
  print("To perform the Caesar cipher with the current settings,")
  print(" 1. Locate each character in the input string")
  print(f" 2. Replace it with the character that is {args.dist} spaces to the {args.dir}")
  print("    a. Pass through spaces, punctuation, other special characters")
  print()
  print(f"{'input'.rjust(10)} = {istr}")
  print(f"{'operation'.rjust(10)} = {args.dir} {args.dist}")
  print()

  # Print az but with spaces between letters
  for c in az:
    print(c, end=' ')
  print("\n")

  wait_enter()

  # If verbosity is over 2, we will step by step explain each word in the string
  if args.verbose > 2:
    # If no instr has been provided, set it to istr (obtained with init_str)...
    if instr is None: instr=istr
    # ...so we can loop through one variable name regardless of how caesar_tutor has been called.
    for c in instr:
      if c.isalpha():
        caesar(c)
        print(f"  '{c}' -> '{rstr}'")
      elif c == "\n":
        print("  newline\n")
      else:
        print(f"  '{c}' -> '{c}'")
    
    print()

# Atbash cipher (reversed letters)
def atbash(instr=None):
  # Need to modify rstr
  global rstr
  # Set wstr based on provided arg to function
  if instr: wstr = instr
  else: wstr = istr

  # Make and use Atbash translation table
  atable = str.maketrans(azAZ, zaZA)
  rstr = wstr.translate(atable)

# Atbash tutorial
def atbash_tutor(instr=None):
  print()
  print(" == Atbash Cipher tutorial ==")
  print()
  print("The Atbash Cipher translates all letters in the string to their complement in the alphabet.")
  print("It is a reciprocal cipher; the same steps are used to encipher and decipher text.")
  print()
  print("To perform the Atbash cipher,")
  print(" 1. Locate each character in the normal alphabet")
  print(" 2. Replace it with the character in the same position in the reversed alphabet")
  print("    a. Pass through spaces, punctuation, other special characters")
  print()
  print(f"{'input'.rjust(10)} = {istr}")
  print()

  # Print az but with spaces between letters
  for c in az:
    print(c, end=' ')
  print()

  # Same for za
  for c in za:
    print(c, end=' ')
  print("\n")

  wait_enter()

  # If verbosity is over 2, we will step by step explain each word in the string
  if args.verbose > 2:
    # If no instr has been provided, set it to istr (obtained with init_str)...
    if instr is None: instr=istr
    # ...so we can loop through one variable name regardless of how caesar_tutor has been called.
    for c in instr:
      if c.isalpha():
        atbash(c)
        print(f"  '{c}' -> '{rstr}'")
      elif c == "\n":
        print("  newline\n")
      else:
        print(f"  '{c}' -> '{c}'")
    
    print()

# A1Z26 cipher (numbered position in alphabet; integrated tutorial)
def a1z26():
  # Need to modify rstr
  global rstr
  # Index of next letter
  idx = 0
  # Resulting string
  rstr = ''

  # Show tutorial explanation if verbosity 2+
  if args.verbose > 1:
    print()
    print(" == A1Z26 Cipher tutorial ==")
    print()
    print("The A1Z26 Cipher represents letters by their numeric position in the alphabet.")
    print("This is summarized in the name; A=1, Z=26.")
    print()
    print("To perform the A1Z26 cipher,")
    print(" 1. Locate each character or number in the input string")
    print(" 2. Replace it with the correlating item of the other type")
    print("    a. If you are enciphering into numbers, add dashes ('-') between successive indices")
    print("    b. If you are deciphering from numbers, remove the dashes between the indices")
    print("    c. Pass through spaces, punctuation, other special characters")
    print()
    print(f"{'input'.rjust(10)} = {istr}")
    print()

    # Print out numbered indices for the alphabet
    for n in range(1,27):
      print(str(n).ljust(3), end='')
    print()

    # Print az to align with the numbers
    for c in az:
      print(str(c).ljust(3), end='')
    print("\n")

    wait_enter()

  # Iterate over individual chars in istr
  for c in istr:
    # Original and new content added this round
    oc = nc = ''
    # Python added case-like match structures in 3.10
    # Using if-elif instead for better compatibility
    if c.isalpha():
      oc += c
      # If the current last character in the resulting string is a digit,
      # we will need a '-' to distinguish the different indices 
      if re.search('[0-9]$', rstr): 
        # Also need to check that the string doesn't end with a newline
        # Not necessary in bash due to different handling of the $ anchor
        if not re.search('\n$', rstr):
          nc += '-'
      # Append the index of the input char
      nc += str(alpha.index(c.lower()))
    elif c.isdigit():
      # Add the digit character to the index variable.
      # Because Python actually cares about types, 
      # we're concatenating as strings then casting back to int
      idx = int( str(idx) + c )
    else:
      if idx != 0:
        oc += str(idx)

      # A dash (or any other character) indicates the end of a numbered index.
      # Add to the resulting string the alphabet character at the current index.
      # This sometimes adds the first item in the array to the string,
      # which doesn't matter since the first item is ''
      nc += alpha[idx]
      # Clear the index variable for next time
      idx = 0
      # If the special character is anything other than a newline,
      # we should report it as part of the original content
      if c != '\n':
        oc += c
        # If the special character is also not a dash, should be passed on to result
        if c != '-': nc += c
    
    # If step-by-step (verb 3+), output original and new content
    if args.verbose > 2 and nc != '':
      # add ' to start of original content for easier readability
      oc = "'" + oc
      print(f"  {oc.rjust(4)}' -> '{nc}'")

    # Append new content to resulting string
    rstr += nc

    # Account for newline characters separately for better step-by-step output
    if c == '\n':
      rstr += c
      if args.verbose > 2:
        print("    newline\n")
  
  # One more append in case the last character is a digit
  rstr += alpha[idx]
  # Report extra append if step-by-step
  if args.verbose > 2:
    if idx != 0:
      oc += "'" + str(idx)
      nc += alpha[idx]
      print(f"  {oc.rjust(4)}' -> '{nc}'")
    # Extra newline
    print()

# Vigenere cipher (offset cipher based on per-letter alphabetic index in provided key)
def vigenere():
  global rstr, args
  # Need to modify rstr and args
  # working string
  wstr = ''
  # resulting string
  rstr = ''
  # index for key
  kin = 0

  # If enciphering with the autokey method, the input string should be appended to the key
  if args.ukey and args.op == 'en':
    args.vkey += istr
  # Remove spaces from key (these may be added from istr)
  args.vkey = nospace(args.vkey)
  
  # Show tutorial explanation if verbosity 2+
  if args.verbose > 1:
    print()
    print(" == Vigenere Cipher tutorial ==")
    print()
    print("The Vigenere Cipher is a more elaborate Caesar cipher in which the offset varies per letter.")
    print("The key provided to the cipher determines this per-letter offset.")
    if args.ukey:
      print()
      print(f"The Autokey variant has been selected, so the plain text should be appended to the key.")
      if args.op == 'en':
        print("It is currently set to *encipher*, so the input text can be immediately appended to the key.")
      else:
        print("It is currently set to *decipher*, so the plain text must be appended to the key as it is deciphered.")
    print()
    print("To perform the Vigenere cipher,")
    print(" 1. Line up each alphabetic input character with the corresponding alpha or numeric character in the key")
    print("    a. Pass through all non-alpha input characters")
    print("    b. Repeat the key as needed")
    print(" 2. Locate the key character's index in the alphabet (or literal value if a digit)")
    print(f" 3. Replace the input character with the character that many characters to the {args.dir}")
    if args.ukey and args.op == 'de':
      print("    a. (Autokey) Add the new character to the end of the key")
    print()
    print(f"{'input'.rjust(10)} = {istr}")
    print(f"{'key'.rjust(10)} = {args.vkey}")
    print(f"{'operation'.rjust(10)} = {args.op}cipher {args.dir}")
    print()

    # Print out numbered indices for the alphabet (like A1Z26)
    for n in range(0,26):
      print(str(n).ljust(3), end='')
    print()

    # Print az to align with the numbers
    for c in az:
      print(str(c).ljust(3), end='')
    print("\n")

    wait_enter()

  process_time = RunTime("processing Vigenere cipher")

  # Iterate over characters in istr
  for c in istr:
    # length of the key
    # this is recalculated each loop for compatibility with deciphering autokey
    klen = len(args.vkey)
    # Check if the character is alphabetic
    if c.isalpha():
      # Find the corresponding offset character using modulo so it wraps around 
      # when istr is longer than vkey, which is a common occurrence (unless autokey)
      k = args.vkey[kin % klen]
      # Assign the dist variable for the offset function to the index of the key char
      # minus one since A should match with 0 offset
      # Since we're using the builtin index function, 
      # we need to manually account for digits in key (Gronsfeld variant)
      if k.isdigit(): args.dist = int(k)
      else: args.dist = alpha.index(k.lower()) - 1
      # Perform offset only on current char from istr
      caesar(c)
      # Internally, the Caesar function uses rstr, so this function uses wstr until the end
      wstr += rstr
      # If deciphering with the autokey method, append deciphered alphanumeric characters to the key
      if args.ukey and args.op == 'de': args.vkey += rstr
      # Increment key index
      kin += 1

      # Step-by-step commentary
      if args.verbose > 2:
        print(f"  '{c}' -> '{rstr}' ( {k} = {args.dist} )")
    else:
      # Key characters should only match up with alpha characters, 
      # so kin is only incremented when encountering alpha in the input (previous branch)
      # Here, we just append whatever other char we have to wstr
      wstr += c

      if args.verbose > 2:
        if c == '\n':
          print("  newline\n")
        else:
          print(f"  '{c}' == '{c}'")
      
  if args.verbose > 2: print()

  process_time.end()

  # Store resulting string
  rstr = wstr

# Beaufort cipher (symmetric substitution cipher similar to the Vigenere cipher)
def beaufort():
  # Need to modify rstr and args
  global rstr, args
  # Resulting string
  rstr = ''

  # index for key
  kin = 0
  # length of the key
  klen = len(args.bkey)

  # Remove spaces from key
  args.bkey = ''.join(args.bkey.split())

  # Show tutorial explanation if verbosity 2+
  if args.verbose > 1:
    print()
    print(" == Beaufort Cipher tutorial ==")
    print()
    print("The Beaufort cipher is similar to the Vigenere cipher in that it processes")
    print("each input character differently based on the provided key.")
    print("It is different in that it is a *reciprocal* cipher,")
    print("so the same procedure is used to encipher and decipher text.")
    print()
    print("To perform the Beaufort cipher:")
    print(" 1. Line up each alphabetic input character with the corresponding alpha or numeric character in the key")
    print("    a. Pass through all non-alpha input characters")
    print("    b. Repeat the key as needed")
    print(" 2. Find the alphabetic indexes for both the input and key characters")
    print(" 3. The resulting character is the character *input* spaces to the L from the *key* character")
    print("    a. To summarize, result = key - input")
    print("    b. Refer to the positive or negative indices listed below depending on the result")
    print()
    print(f"{'input'.rjust(10)} = {istr}")
    print(f"{'key'.rjust(10)} = {args.bkey}")
    print(f"{'formula'.rjust(10)} = result = key - input")
    print()

    # Print out positive indices for the alphabet starting at 0
    for n in range(0,26):
      print(str(n).ljust(3), end='')
    print(" - positive indices")

    # Print az to align with the numbers
    for c in az:
      print(str(c).ljust(3), end='')
    print()

    # Print out negative indices for the alphabet
    for n in range(26,0,-1):
      print(str(n).ljust(3), end='')
    print(" - negative indices \n")

    wait_enter()

  # Iterate over characters in istr
  for c in istr:
    # Check if the character is alphabetic
    if c.isalpha():
      # Find the corresponding offset character using modulo so it wraps around 
      # when istr is longer than vkey, which is a common occurrence (unless autokey)
      k = args.bkey[kin % klen]
      # Find alphabetic indices for key and input character
      aik = alpha.index(k.lower())
      aic = alpha.index(c.lower())
      # The index of the result character is 
      # the index of the key character minus the index of the input character
      idx = aik - aic
      # Add the result character to the result string
      nc = alpha[idx]
      rstr += nc
      # Increment key index
      kin += 1

      # Step-by-step commentary
      if args.verbose > 2:
        print(f"  '{c}' -> '{nc}' -- ( {k}={aik} ) - ( {c}={aic} ) = ( {idx}={nc} )")
    else:
      # Key characters should only match up with alpha characters, 
      # so kin is only incremented when encountering alpha in the input (previous branch)
      # Here, we just append whatever other char we have to wstr
      rstr += c

      if args.verbose > 2:
        if c == '\n':
          print("  newline\n")
        else:
          print(f"  '{c}' -> '{c}'")
  
  if args.verbose > 2: print()

# Keyword cipher (monoalphabetic substitution cipher determined by a keyword)
def keyword(instr=None):
  # Need to modify rstr
  global rstr
  # Set wstr based on provided arg to function
  if instr: wstr = instr
  else: wstr = istr

  # Construct upper and lowercase cipher alphabet strings
  kz = ''.join(alpha)
  KZ = kz.upper()
  kzKZ = kz + KZ

  # Actual operation depends on whether we're enciphering or deciphering
  if args.op == 'de':
    # To decipher, translate k-z and K-Z back to a-z and A-Z
    # Construct and use translation table
    ktable = str.maketrans(kzKZ, azAZ)
    rstr = wstr.translate(ktable)
  else:
    # To encipher (default), translate a-z and A-Z to k-z and K-Z
    ktable = str.maketrans(azAZ, kzKZ)
    rstr = wstr.translate(ktable)

# Mixed alphabet tutorial, used by Keyword, Playfiar, and Bifid
def malpha_tutor(key=None):
  print("To construct the mixed alphabet:")
  print(f" 1. Write out all {color.bl}unique{color.nm} letters in the provided key: {color.bl}{key}{color.nm}")
  print(f"    a. Only the {color.bl}first occurrence{color.nm} of duplicate letters should remain")
  print(f" 2. After the key, write out all remaining letters of the alphabet in their regular order")
  print()
  print("You may prefer to think of it in the way that the script accomplishes it:")
  print(" 1. Start with the normal alphabet")
  print(f" 2. For each {color.bl}unique{color.nm} letter in the provided key:")
  print(f"    a. Move it to just before the first letter in the {color.bl}untouched{color.nm} alphabet")
  print()
  print(f"{'key'.rjust(10)} = {key}")
  print()

  wait_enter("Construct the alphabet")

# Keyword tutorial
def keyword_tutor(instr=None):
  print()
  print(" == Keyword Cipher tutorial ==")
  print()
  print("The Keyword cipher is a monalphabetic substitution cipher that")
  print("uses the provided key to construct a mixed cipher alphabet.")
  print()
  malpha_tutor(args.kkey)
  print("Now that we have the mixed alphabet constructed:")
  if args.op == 'de':
    print(" 1. Locate each input cahracter in the mixed alphabet")
    print(" 2. Replace it with the character in the same position in the normal alphabet")
  else:
    print(" 1. Locate each input character in the normal alphabet")
    print(" 2. Replace it with the character in the same position in the mixed alphabet")
  print()
  print(f"{'input'.rjust(10)} = {istr}")
  print()

  # Print az but with spaces between letters
  for c in az:
    print(c, end=' ')
  print()

  # Same for alpha (which has been mixed with the key)
  for c in alpha:
    print(c, end=' ')
  print("\n")

  wait_enter()

  # If verbosity is over 2, we will step by step explain each word in the string
  if args.verbose > 2:
    # If no instr has been provided, set it to istr (obtained with init_str)...
    if instr is None: instr=istr
    # ...so we can loop through one variable name regardless of how caesar_tutor has been called.
    for c in instr:
      if c.isalpha():
        keyword(c)
        print(f"  '{c}' -> '{rstr}'")
      elif c == "\n":
        print("  newline\n")
      else:
        print(f"  '{c}' -> '{c}'")
    
    print()

# Polybius square tutorial, used by Playfair and Bifid
def psquare_tutor():
  print("To construct the Polybius square in a 5x5 grid:")
  print(f" 1. Remove the letter '{color.bl}j{color.nm}' from the alphabet")
  print("    a. We only have 25 total spaces in the square, so we need to drop 1 letter")
  print(f"    b. Instances of '{color.bl}j{color.nm}' will be replaced with '{color.bl}i{color.nm}'")
  print(f" 2. Place the {color.bl}first 5{color.nm} characters in the alphabet on the first row")
  print(f" 3. Place the {color.bl}next 5{color.nm} characters in the second row and {color.bl}repeat{color.nm} for the other 3 rows")
  print()
  # Print mixed alphabet with spaces between letters
  for c in alpha:
    print(c, end=' ')
  print("\n")

  wait_enter("Construct the square")

# Playfair cipher (digram substitution cipher using Polybius square)
def playfair():
  # Need to modify rstr
  global rstr
  # Playfair considers 'j' as 'i' and doesn't like spaces
  # Store substituted string in wstr
  wstr = nospace(istr.translate(no_j))
  # Resulting string
  rstr = ''

  # Show tutorial explanation if verbosity 2+
  if args.verbose > 1:
    print()
    print(" == Playfair Cipher tutorial ==")
    print()
    print("The Playfair cipher is a digram substitution cipher that uses")
    print("a mixed alphabet and Polybius square to process two characters at once.")
    print()
    malpha_tutor(args.pkey)
    psquare_tutor()
    print("Now that we have the Polybius square, we can perform the Playfair cipher:")
    print(" 1. Replace all j's in the input string with i")
    print(" 2. Loop through each pair of letters in the input string")
    print("    a. If you encounter a pair of the same letter, add 'x' in between")
    print("       ('falls' -> 'fa lx ls')")
    print("    b. If you reach the end of the string with just one letter, add an extra 'x'")
    print("       ('welcome' -> 'we lc om ex')")
    print(" 3. Locate the pair of letters in the Polybius square and find which rule it matches:")
    if args.op == 'de':
      print("    a. If they appear in the same column, replace each letter with the one immediately above it")
      print(f"       ('{get2d(2,2)}{get2d(2,3)}' -> '{get2d(2,1)}{get2d(2,2)}')")
      print("    b. If they appear in the same row, replace each letter with the one immediately to the left")
      print(f"       ('{get2d(4,2)}{get2d(0,2)}' -> '{get2d(3,2)}{get2d(4,2)}')")
    else:
      print("    a. If they appear in the same column, replace each letter with the one immediately below it")
      print(f"       ('{get2d(2,1)}{get2d(2,2)}' -> '{get2d(2,2)}{get2d(2,3)}')")
      print("    b. If they appear in the same row, replace each letter with the one immediately to the right")
      print(f"       ('{get2d(3,2)}{get2d(4,2)}' -> '{get2d(4,2)}{get2d(0,2)}')")
    print("    c. If neither of those match, imagine a rectangle around them and select the other corners of it")
    print("       Each letter should be replaced with the other corner that is on the same row")
    print(f"       ('{get2d(4,2)}{get2d(0,1)}' -> '{get2d(0,2)}{get2d(4,1)}')")
    print(" 4. (opt.) The script doesn't do this because it is difficult to do programatically,")
    print("    but you may wish to add spaces and special characters back in where they make sense")
    print()
    print(f"{'input'.rjust(10)} = {istr}")
    print()

    # Print out 2d Polybius square
    print2d()
    print()

    wait_enter()

  # Loop through individual characters in wstr.
  # Using while loop because the step size is irregular based on factors in the loop
  # and Python doesn't allow modifying the iterator in the loop
  i = 0
  while i < len(wstr):
    # Get current and next letters in wstr
    try:
      cur = wstr[i]
    except:
      # If can't get cur, just increment i and go to next loop run
      i += 1
      continue
    try:
      nxt = wstr[i + 1]
    except:
      # If can't get nxt, probably end of string
      # For end of string, nxt should be blank (to be padded with 'x') and continue with that
      nxt = ''

    if cur.isalpha():
      # If the next value is empty, it's the end of the string
      if nxt == '':
        # Pad with 'x'
        nxt = 'x'
        # We are processing one char this loop run
        i += 1
      # If the two characters are the same
      elif nxt == cur:
        # Pad with 'x'
        nxt = 'x'
        # We are processing one char this loop run
        i += 1
      # If the next value is another alpha character, we will skip it next loop
      elif nxt.isalpha():
        # We are processing two chars this loop run
        i += 2
      else:
        # If not a letter, we need to delete it from the string
        # Reassign wstr to two slices of itself excluding the offending char
        wstr = wstr[:i + 1] + wstr[i + 2:]
        # We are processing no chars this loop run
        # No change to i and we should continue right on to the next loop iteration
        continue
      
      # Get current and next indices
      try:
        ci = alpha.index(cur.lower())
        ni = alpha.index(nxt.lower())
      except:
        # If errors, continue with loop
        errprint(f"Couldn't get index for {cur} or {nxt}")
        continue

      # Get current and next x/y position
      # Observe the layout of print2d() if this doesn't make sense
      # In Python, // is integer division
      cx = ci % 5
      cy = ci // 5
      nx = ni % 5
      ny = ni // 5

      # New content added this loop run
      nc = ''

      # If they're in the same "column"
      if cx == nx:
        # Each character is "up" one row for deciphering (down for enciphering)
        # In this and each other branch, append space at end to show digrams
        if args.op == 'de':
          nc = get2d(cx, cy - 1) + get2d(nx, ny - 1)
        else:
          nc = get2d(cx, cy + 1) + get2d(nx, ny + 1)
      # If they're in the same "row"
      elif cy == ny:
        # Each character is to the left one column for deciphering (right for enciphering)
        if args.op == 'de':
          nc = get2d(cx - 1, cy) + get2d(nx - 1, ny)
        else:
          nc = get2d(cx + 1, cy) + get2d(nx + 1, ny)
      # If neither x nor y match between them
      else:
        # Swap x-values, same for (en/de)ciphering
        nc = get2d(nx, cy) + get2d(cx, ny)
      
      rstr += nc + ' '

      # Step-by-step commentary if verbose enough
      if args.verbose > 2:
        print(f"  '{cur}{nxt}' -> '{nc}'")
    else:
      # If cur is not a letter, we need to delete it from the string
      # Reassign wstr to two slices of itself excluding the offending char
      wstr = wstr[:i] + wstr[i + 1:]
      # We are processing no chars this loop run
      # No change to i and we should continue right on to the next loop iteration
      continue

  if args.verbose > 2: print()

# Bifid cipher (digraphic cipher using Polybius square like Playfair)
def bifid():
  # Need to modify rstr
  global rstr
  # Bifid (like Playfair) considers 'j' as 'i'
  # Store substituted string in wstr
  wstr = istr.translate(no_j)
  # Resulting string
  rstr = ''

  if args.verbose > 1:
    print()
    print(" == Bifid Cipher tutorial ==")
    print()
    print("The Bifid cipher is another somewhat complicated cipher that uses")
    print("a mixed alphabet and Polybius square.")
    print(f"The cipher then intersperses {color.bl}row{color.nm} (Y) and {color.bl}column{color.nm} (X) values to mask the contents.")
    print()
    malpha_tutor(args.ikey)
    psquare_tutor()
    print("Now that we have the Polybius square, we can start with the cipher by collecting row/column values:")
    print(" 1. For each letter in the input string, locate it in the Polybius square")
    if args.op == 'de':
      print(f" 2. Add the row and column values to a {color.bl}single list{color.nm} of coordinates")
    else:
      print(f" 2. Add the row and column values to {color.bl}separate lists{color.nm}")
    print()
    print(f"{'input'.rjust(10)} = {istr}")
    print()

    # Print out 2d Polybius square
    print2d()
    print()

    wait_enter("Create the list(s)")

  # Empty arrays for X and Y values
  arrx = []
  arry = []
  # Full array for y-values interspersed with x-values
  fullarr = []

  # Looping through individual letters
  # Unlike Playfair, don't need cur and nxt both so simpler loop syntax
  for c in wstr:
    # If it's an alphabetic character, get the index
    if c.isalpha():
      ci = alpha.index(c.lower())
      # Get current and next x/y position
      # Observe the layout of print2d() if this doesn't make sense
      # In Python, // is integer division
      cx = ci % 5
      cy = ci // 5
      # Assign to arrays depending on operation
      if args.op == 'de':
        fullarr += [ cy, cx ]
      else:
        arrx.append(cx)
        arry.append(cy)

  if args.op == 'de':
    # Check and report potential problem if the full array length is odd
    if len(fullarr) % 2:
      errprint("bifid(): full array has an odd number of items; result may be incorrect")

    if args.verbose > 1:
      print("With the full list of coordinates, we need to split them up into row and column values:")
      print(f" 1. Add the {color.bl}first {args.period}{color.nm} (current period value) numbers to the list of row values")
      print(f" 2. Add the {color.bl}next {args.period}{color.nm} numbers to the list of column values")
      print(f" 3. Repeat until you have less than {color.bl}{args.period * 2}{color.nm} values left")
      print(f" 4. Add the {color.bl}first half{color.nm} of the remaining numbers to the list of {color.bl}row values{color.nm},")
      print(f"    the {color.bl}second half{color.nm} to the list of {color.bl}column values{color.nm}")
      print()
      print(f"{'period'.rjust(10)} = {args.period}")
      print(f"{'coords'.rjust(10)} = ", end='')
      # Print full array with spaces between numbers
      for c in fullarr:
        print(str(c), end=' ')
      print("\n")

      wait_enter("Split the list")
      
    # Copy period to another variable (interval) so we can edit it if needed
    # to account for string lengths that aren't a multiple of period
    ivl = args.period
    # Loop through every other multiple of period
    for i in range(0, len(fullarr), args.period * 2):
      # Each run processes the next period * 2 items.
      # We need to check if this will place it past the end of the array.
      # diff is the number of items it will want to process this run
      # that do NOT have array elements corresponding to them.
      diff = i + (args.period * 2) - len(fullarr)
      # If it is negative or zero, we're good.
      if diff > 0:
        # If it is positive, we need to alter the behavior so that
        # half of the remaining array elements are added as y- and x-values each.
        # For this run, the interval on which it operates needs to be
        # diff / 2 less than the typical period (with integer division)
        ivl = args.period - (diff // 2)
      # Add the next ivl items as y-values (rows)
      arry += fullarr[i : i + ivl]
      # Add the next ivl items after that as x-values (columns)
      arrx += fullarr[i + ivl : i + (2 * ivl)]
    
    if args.verbose > 1:
      print("The two lists can now be read directly as row/column values for the result.")
      print()
      print(f"{'rows'.rjust(10)} = ", end='')
      for c in arry:
        print(str(c), end=' ')
      print()
      print(f"{'columns'.rjust(10)} = ", end='')
      for c in arrx:
        print(str(c), end=' ')
      print("\n")
  else:
    if args.verbose > 1:
      print("With the lists of row and column values, we need to unify them into one list:")
      print(f" 1. Add the first {args.period} (current period value) {color.bl}row{color.nm} values to the full list")
      print(f" 2. Add the first {args.period} {color.bl}column{color.nm} values to the full list")
      print(f" 3. {color.bl}Repeat{color.nm} until all values have been added to the full list")
      print()
      print(f"{'period'.rjust(10)} = {args.period}")
      print(f"{'rows'.rjust(10)} = ", end='')
      for c in arry:
        print(str(c), end=' ')
      print()
      print(f"{'columns'.rjust(10)} = ", end='')
      for c in arrx:
        print(str(c), end=' ')
      print("\n")

      wait_enter("Create the full list")
      
    # Loop through values from 0 to the x array length in period increments
    for i in range(0, len(arrx), args.period):
      # Full array should be the next period y-values, then that many x-values,
      # then the next period y- then x-values until arry/arrx are exhausted
      fullarr += arry[i : i + args.period] + arrx[i : i + args.period]
    
    if args.verbose > 1:
      print("The full list that we now have is the list of row and column values for the result.")
      print(f"For example, the first result character is at row={fullarr[0]}, col={fullarr[1]}")
      print()
      print(f"{'coords'.rjust(10)} = ", end='')
      # Print full array with spaces between numbers
      for c in fullarr:
        print(str(c), end=' ')
      print("\n")
  
  if args.verbose > 1:
    print("You may also want to refer back to the input string and add back")
    print("spaces and special characters in the same positions in the resulting string.")
    print()
    print(f"{'input'.rjust(10)} = {istr}")
    print()
    print2d()
    print()
    
    wait_enter()
  
  # Array index
  ai = 0

  # Looping through the original string again to simplify spaces/special chars
  for c in wstr:
    nc = ''
    # If it's an alphabetic character, add substitution character to result
    if c.isalpha():
      if args.op == 'de':
        # Get x/y values depending on operation
        cy = arry[ai]
        cx = arrx[ai]
        ai += 1
      else:
        # The x,y values for enciphering are the next two items in the full array
        cy = fullarr[ai]
        cx = fullarr[ai + 1]
        # Increment index variable by 2
        ai += 2
      nc = get2d(cx, cy)
      if args.verbose > 2:
        print(f"  '{c}' -> '{nc}' (r={cy}, c={cx})")
    else:
      # If not an alphabetic char, add whatever else it is to the result
      nc = c
      if args.verbose > 2:
        if c == '\n':
          print("  newline\n")
        else:
          print(f"  '{c}' -> '{nc}'")
    rstr += nc
  
  if args.verbose > 2: print()

# Customized monoalphabetic substitution
def substitute():
  # Need to modify rstr
  global rstr
  # Initialize wstr
  wstr = istr

  # Normal and substitution strings
  az = ''
  sz = ''
  # Variable for how to handle case for the substitution function
  # This determines the case of SUBSTITUTED letters; untouched letters will be the opposite case
  # Default to capitalizing input and replacing affected characters with lowercase
  subcase = "C"
  
  # Iterate over the substitution directives to read file(s)
  # and separate source from destination substitution
  for arg in args.sub:
    if re.search('^file=', arg):
      # Grab filename as portion of argument after = sign
      fname = arg.split('=')[1]
      if args.verbose > 1: print(f"Will read from file: {fname}")
      # Try-except in case the filename is invalid or unreadable
      try:
        infile = open(fname, "r")
      except:
        infile = ''
        errprint("Couldn't open file")
      # Add each line in the file as a new argument to process
      for line in infile:
        args.sub.append(line.rstrip())
    elif re.search('^case=', arg):
      # Grab case designator from argument and canonicalize
      try:
        subcase = canoncase(arg.split('=')[1])
      except ValueError:
        errprint("Will use default case")
    # Grab specified src and dst directives
    elif re.search('^(src|from)=', arg):
      newsrc = arg.split('=')[1]
      az += newsrc
    elif re.search('^(dst|to)=', arg):
      newdst = arg.split('=')[1]
      sz += newdst
    # Verify that the first two characters are alphabetical
    elif arg[0].isalpha() and arg[1].isalpha():
      if args.verbose > 1: print(f"Will substitute {arg}")
      az += arg[0]
      sz += arg[1]
      #subdict[arg[1]] = arg[0]
    else:
      errprint(f"Ignoring unintelligible argument: {arg}")
  
  if len(az) != len(sz):
    errprint("Source and destination sets are not the same length;")
    errprint("will truncate to the shorter length")
    az = az[0:len(sz)]
    sz = sz[0:len(az)]

  # List of tuples for substitution directives
  # First is the *substitution* character, that is, the plaintext letters that have been deciphered
  # That way it can be easily sorted to show which target substitution letters have not been used
  sortpairs = sorted(zip([*sz], [*az]))
  # This gets each list back into the correct string
  sz, az = [ ''.join(list(tuple)) for tuple in zip(*sortpairs) ]

  if args.verbose > 0:
    print("Custom substiuttion alphabets:")
    print(f"from: {az}")
    print(f"  to: {sz}")

  # Initialize various cases of the original and substitution lists
  azAZ = az.lower() + az.upper()
  szsz = sz.lower() + sz.lower()
  szSZ = sz.lower() + sz.upper()
  SZSZ = sz.upper() + sz.upper()
  # Variable for the selected sz version
  selsz = ""

  # If subcase is none, then we will translate preserving case
  # This is NOT the default as it obfuscates which parts of the string were translated
  # and which still need analyzed to attempt a substitution
  if subcase == "N":
    if args.verbose > 1: print("Preserving original case")
    selsz = szSZ
  # If subcase is capital, then the input should be lowercase
  # and all substitutions should be uppercase
  elif subcase == "C":
    if args.verbose > 1: print("Replacements will be in uppercase")
    wstr = istr.lower()
    selsz = SZSZ
  # If subcase is small, then the input should be uppercase
  # and all substitutions should be lowercase
  elif subcase == "S":
    if args.verbose > 1: print("Replacements will be in lowercase")
    wstr = istr.upper()
    selsz = szsz

  # Make and use customized translation table
  stable = str.maketrans(azAZ, selsz)
  rstr = wstr.translate(stable)

# Generate a random monoalphabetic cipher operation
def randomize():
  # Need to modify rstr, rz needs to be seen outside of this function
  global rstr, rz
  wstr = istr

  # This will be the substitution alphabet
  rz = ''

  # If the Caesar flag was provided, do a random Caesar offset
  if args.caesar:
    if args.verbose > 1: print("Generating random Caesar offset")
    # Generate random number between 1 and 25
    args.dist = random.randint(1,25)
    # Always go right (although direction doesn't reall matter here)
    args.dir = "R"
    # Perform Caesar operation
    caesar()
    # Set rz so the report works
    rz = f"Caesar {args.dist}"
  # If no other recognized flags provided, do a fully random monoalphabetic substiuttion
  else:
    if args.verbose > 1: print("Generating random monoalphabetic substitution")
    # Remove empty element from alphabet list
    alpha.remove('')
    # Duplicate and shuffle the normal alphabet list
    ralpha = alpha.copy()
    random.shuffle(ralpha)

    # Loop through regular alphabet to check for non-substituted letters.
    # The shuffle() function doesn't guarantee that every item will move from its original position.
    # However, for our purposes, we would like each alphabetic letter to
    # be substituted for a different letter.
    # This will accomplish that with one rare limitation:
    # if 'z' is not moved this will not be able to correct it.
    for c in alpha:
      # Substitution character should be the first item in the
      # remaining randomized alphabet array
      sc = ralpha[0]
      # If the substitution letter is the same and there are enough available random letters,
      if c == sc and len(ralpha) > 1:
        if args.verbose > 2: print("Same letter, looking ahead")
        # use the next item in the array as the substitution letter instead
        sc = ralpha[1]
      # Add the substitution letter to the substitution alphabet string
      rz += sc
      if args.verbose > 2: print(f"{c} -> {sc}")
      # Remove the substitution character from the array
      ralpha.remove(sc)
    
    if args.verbose > 1:
      print(f"Finalized substitution alphabet: {rz}")

    # Create uppercase and mixed substitution strings
    RZ = rz.upper()
    rzRZ = rz + RZ

    # Make and use randomized translation table
    rtable = str.maketrans(azAZ, rzRZ)
    rstr = wstr.translate(rtable)

# Report current state of the resulting string, stripping spaces if requested
def report():
  # Different approach than Bash version
  # Using internal output var to modify output
  output = rstr
  
  # If strip spaces requested, perform that change here
  if args.strip: 
    output = nospace(output)
  
  # Handle upper/lower case
  if args.case == "C":
    output = output.upper()
  elif args.case == "S":
    output = output.lower()
  
  # Print the processed output string
  print(output)


########## End of function declarations


########## Start of execution


# Initialize variables from provided arguments
get_args()

# Initialize strings
init_str()
alphinit()

# Determine contents of the input strong from the contains() function
cont = contains()

# Perform each cipher based on the contents of the input
# or confirmation to continue anyway

# Random cipher substitution
if args.rand:
  randomize()
  if args.verbose: print(f"  Processed with {color.bl}random monoalphabetic substitution{color.nm} ({rz}):")
  report()
  sys.exit()

# Caesar
if args.caesar:
  this_cipher = Caesar(istr, args.dist, args.dir, args.brute)
  this_cipher.run()

# Atbash
if args.atbash:
  this_cipher = Atbash(istr)
  this_cipher.run()

# A1Z26
if args.a1z26:
  this_cipher = A1Z26(istr)
  this_cipher.run()

# Vigenere
if args.vkey:
  # Here in Python, don't need to verify the key exists since argparse did it already
  if cont == "letters" or confirm():
    args.brute = False
    vigenere()
    if args.verbose: print(f"  Processed with {color.bl}Vigenere {args.dir} {args.vkey}{color.nm}:")
    report()

# Beaufort
if args.bkey:
  if cont == "letters" or confirm():
    alphinit('b')
    beaufort()
    if args.verbose: print(f"  Processed with {color.bl}Beaufort {args.bkey}{color.nm}:")
    report()

# Keyword
if args.kkey:
  if cont == "letters" or confirm():
    alphinit('k')
    # Run Keyword Tutor if verbosity is 2+
    if args.verbose > 1: keyword_tutor()
    keyword()
    if args.verbose: print(f"  Processed with {color.bl}Keyword {args.kkey}{color.nm}:")
    report()

# Playfair
if args.pkey:
  if cont == "letters" or confirm():
    alphinit('p')
    playfair()
    if args.verbose: print(f"  Processed with {color.bl}Playfair {args.pkey}{color.nm}:")
    report()

# Bifid
if args.ikey:
  if cont == "letters" or confirm():
    alphinit('i')
    bifid()
    if args.verbose: print(f"  Processed with {color.bl}Bifid {args.ikey} {args.period}{color.nm}:")
    report()

# Custom substitution
if args.sub:
  if cont == "letters" or confirm():
    if args.verbose > 1: print(args.sub)
    substitute()
    if args.verbose: print(f"  Processed with custom monoalphabetic substitution:")
    report()
